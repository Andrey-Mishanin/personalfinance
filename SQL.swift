//
//  SQL.swift
//  PersonalFinance
//
//  Created by Andrey Mishanin on 13/09/2020.
//  Copyright © 2020 Andrey Mishanin. All rights reserved.
//

import Foundation
import SQLite3

public func openDatabase(url: URL) -> OpaquePointer? {
  var db: OpaquePointer?
  guard sqlite3_open(url.path, &db) == SQLITE_OK else {
    print("Unable to open database")
    return nil
  }
  print("Opened database at \(url.path)")
  return db
}

class SQLiteDecoder: Decoder {
  var codingPath: [CodingKey] = []

  var userInfo: [CodingUserInfoKey : Any] = [:]

  private let queryStatement: OpaquePointer

  init(queryStatement: OpaquePointer) {
    self.queryStatement = queryStatement
  }

  func container<Key>(keyedBy type: Key.Type) throws -> KeyedDecodingContainer<Key> where Key : CodingKey {
    KeyedDecodingContainer<Key>(SQLiteKeyDecodingContainer(queryStatement: queryStatement))
  }

  func unkeyedContainer() throws -> UnkeyedDecodingContainer {
    fatalError()
  }

  func singleValueContainer() throws -> SingleValueDecodingContainer {
    fatalError()
  }

  func decode<T : Decodable>(_ t: T.Type) throws -> T {
    return try T.init(from: self)
  }
}

enum SQLiteDecodingError: Error {
  case unknown
}

struct SQLiteKeyDecodingContainer<Key>: KeyedDecodingContainerProtocol where Key : CodingKey {
  var codingPath: [CodingKey] = []
  var allKeys: [Key] = []

  private let queryStatement: OpaquePointer

  init(queryStatement: OpaquePointer) {
    self.queryStatement = queryStatement
  }

  func contains(_ key: Key) -> Bool {
    sqlite3_column_text(queryStatement, Int32(key.intValue!)) != nil
  }

  func decodeNil(forKey key: Key) throws -> Bool {
    sqlite3_column_text(queryStatement, Int32(key.intValue!)) == nil
  }

  func decode(_ type: Bool.Type, forKey key: Key) throws -> Bool {
    fatalError()
  }

  func decode(_ type: String.Type, forKey key: Key) throws -> String {
    guard let cStr = sqlite3_column_text(queryStatement, Int32(key.intValue!)) else {
      throw SQLiteDecodingError.unknown
    }
    return String(cString: cStr)
  }

  func decode(_ type: Double.Type, forKey key: Key) throws -> Double {
    sqlite3_column_double(queryStatement, Int32(key.intValue!))
  }

  func decode(_ type: Float.Type, forKey key: Key) throws -> Float {
    fatalError()
  }

  func decode(_ type: Int.Type, forKey key: Key) throws -> Int {
    Int(sqlite3_column_int(queryStatement, Int32(key.intValue!)))
  }

  func decode(_ type: Int8.Type, forKey key: Key) throws -> Int8 {
    fatalError()
  }

  func decode(_ type: Int16.Type, forKey key: Key) throws -> Int16 {
    fatalError()
  }

  func decode(_ type: Int32.Type, forKey key: Key) throws -> Int32 {
    fatalError()
  }

  func decode(_ type: Int64.Type, forKey key: Key) throws -> Int64 {
    fatalError()
  }

  func decode(_ type: UInt.Type, forKey key: Key) throws -> UInt {
    fatalError()
  }

  func decode(_ type: UInt8.Type, forKey key: Key) throws -> UInt8 {
    fatalError()
  }

  func decode(_ type: UInt16.Type, forKey key: Key) throws -> UInt16 {
    fatalError()
  }

  func decode(_ type: UInt32.Type, forKey key: Key) throws -> UInt32 {
    fatalError()
  }

  func decode(_ type: UInt64.Type, forKey key: Key) throws -> UInt64 {
    fatalError()
  }

  func decode<T>(_ type: T.Type, forKey key: Key) throws -> T where T : Decodable {
    fatalError()
  }

  func nestedContainer<NestedKey>(keyedBy type: NestedKey.Type, forKey key: Key) throws -> KeyedDecodingContainer<NestedKey> where NestedKey : CodingKey {
    fatalError()
  }

  func nestedUnkeyedContainer(forKey key: Key) throws -> UnkeyedDecodingContainer {
    fatalError()
  }

  func superDecoder() throws -> Decoder {
    fatalError()
  }

  func superDecoder(forKey key: Key) throws -> Decoder {
    fatalError()
  }
}

public func readRows<T: Decodable>(from db: OpaquePointer, query: String) -> [T]? {
  var queryStatement: OpaquePointer?

  guard sqlite3_prepare_v2(db, query, -1, &queryStatement, nil) == SQLITE_OK else {
    let errorMsg = String(cString: sqlite3_errmsg(db))
    print("Query not prepared: \(errorMsg)")
    return nil
  }

  defer {
    sqlite3_finalize(queryStatement)
  }

  var results: [T] = []
  while sqlite3_step(queryStatement) == SQLITE_ROW {
    let transaction = try? SQLiteDecoder(queryStatement: queryStatement!).decode(T.self)
    results.append(transaction!)
  }

  return results
}
