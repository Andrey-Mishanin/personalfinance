//
//  TableView.swift
//  PersonalFinance
//
//  Created by Andrey Mishanin on 15/11/2020.
//  Copyright © 2020 Andrey Mishanin. All rights reserved.
//

import SwiftUI

public struct DynamicTableView: View {
  @ObservedObject var observable: ObservableTableUI

  public init(observable: ObservableTableUI) {
    self.observable = observable
  }

  public var body: some View {
    TableView(model: observable.model, observer: observable.observer)
      .onAppear { observable.observer.uiWillAppear() }
  }
}

struct TableView: View {
  let model: TableViewModel
  unowned let observer: TableUIObserver

  var body: some View {
    List {
      ForEach(model.sections) { section in
        Section(header: Text(section.name ?? "")) {
          ForEach(section.rows) { row in
            TableRowView(row: row, sectionID: section.id, observer: observer)
          }
        }
      }
    }
  }
}

struct TableRowView: View {
  let row: TableViewModel.Row
  let sectionID: Int
  unowned let observer: TableUIObserver

  var body: some View {
    HStack {
      if let badgeColor = row.style.badgeColor {
        SwiftUI.Color(badgeColor)
          .frame(width: 5)
      }
      Text(row.title.string)
      Spacer()
      Text(row.detail.string)
        .bold()
        .foregroundColor(textColor(from: row.detail.attributes))
      Image(systemName: "chevron.right")
    }
    .contentShape(Rectangle())
    .onTapGesture {
      observer.rowWasSelected(at: IndexPath(item: row.id, section: sectionID))
    }
  }
}

private func textColor(from attrs: [AttributedString.Attribute]) -> SwiftUI.Color? {
  for attr in attrs {
    if case let .textColor(color) = attr {
      return SwiftUI.Color(color)
    }
  }
  return nil
}

struct TableView_Previews: PreviewProvider {
  final class Observer: TableUIObserver {
    func uiWillAppear() {}
    func rowWasSelected(at indexPath: IndexPath) {}
    func row(at index: Int, didChangeDetailTextTo detailText: String) {}
  }

  static var previews: some View {
    TableView(model: expectedVM, observer: Observer())
  }
}

private let expectedVM = makeTableVM([
  (name: "March 2019", itemVMs: [
    (info: "", amount: "+£2,036.00", color: .blue),
  ]),
  (name: "January 2018", itemVMs: [
    (info: "Durex", amount: "₽480.00", color: .purple),
    (info: "Спазмалгон", amount: "₽42.30", color: .purple),
  ])
])

private typealias TransactionVM = (info: String, amount: String, color: Color)
private typealias ListVM = [ (name: String, itemVMs: [TransactionVM]) ]
private func makeTableVM(_ listVM: ListVM) -> TableViewModel {
  let sections = listVM.enumerated().map {
    TableViewModel.Section(name: $0.1.name, rows: $0.1.itemVMs.enumerated().map {
      TableViewModel.Row(title: $0.1.info.withAttributes([]),
                         detail: $0.1.amount.withAttributes(rightAlignedAttrs(for: $0.1.amount)),
                         style: .form(editableStyle: .none, isFirstResponder: false, badgeColor: $0.1.color),
                         selectionStyle: .gray,
                         accessoryType: .disclosureIndicator,
                         id: $0.0)
    }, id: $0.0)
  }
  return TableViewModel(sections: sections)
}

private func rightAlignedAttrs(for amount: String) -> [AttributedString.Attribute] {
  [
    .font(.boldSystemFont(ofSize: 17)),
    .paragraphStyle(.right),
  ] + (amount.starts(with: "+") ? [.textColor(.systemGreen)] : [])
}
