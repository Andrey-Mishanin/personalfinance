//
//  NSAttributedStringExtensions.swift
//  PersonalFinance
//
//  Created by Andrey Mishanin on 10/04/2017.
//  Copyright © 2017 Andrey Mishanin. All rights reserved.
//

import Foundation

public extension String {
  func withAttributes(_ attributes: [NSAttributedString.Key: Any] = [:]) -> NSAttributedString {
    return NSAttributedString(string: self, attributes: attributes)
  }

  func withAttributes(_ attributes: [AttributedString.Attribute]) -> AttributedString {
    return AttributedString(string: self, attributes: attributes)
  }
}

public extension ParagraphStyle {
  static func with(alignment: TextAlignment) -> ParagraphStyle {
    let mutableStyle = MutableParagraphStyle()
    mutableStyle.alignment = alignment
    return mutableStyle
  }
}
