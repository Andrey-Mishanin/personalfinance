//
//  PlatformTypes.swift
//  PersonalFinance
//
//  Created by Andrey Mishanin on 10/04/2016.
//  Copyright © 2016 Andrey Mishanin. All rights reserved.
//

import Foundation

public typealias Color = NSColor
public typealias ParagraphStyle = NSParagraphStyle
public typealias MutableParagraphStyle = NSMutableParagraphStyle
public typealias TextAlignment = NSTextAlignment
public typealias Font = NSFont
