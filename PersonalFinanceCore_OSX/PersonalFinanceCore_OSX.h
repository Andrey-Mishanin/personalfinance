//
//  PersonalFinanceCore_OSX.h
//  PersonalFinanceCore_OSX
//
//  Created by Andrey Mishanin on 10/04/2016.
//  Copyright © 2016 Andrey Mishanin. All rights reserved.
//

#import <Cocoa/Cocoa.h>

//! Project version number for PersonalFinanceCore_OSX.
FOUNDATION_EXPORT double PersonalFinanceCore_OSXVersionNumber;

//! Project version string for PersonalFinanceCore_OSX.
FOUNDATION_EXPORT const unsigned char PersonalFinanceCore_OSXVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <PersonalFinanceCore_OSX/PublicHeader.h>


