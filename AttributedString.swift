//
//  AttributedString.swift
//  PersonalFinance
//
//  Created by Andrey Mishanin on 24/04/2017.
//  Copyright © 2017 Andrey Mishanin. All rights reserved.
//

import Foundation

public struct AttributedString: Equatable {
  public enum Attribute: Equatable {
    case font(Font)
    case paragraphStyle(TextAlignment)
    case textColor(Color)
    case baselineOffset(Float)
  }

  public let string: String
  public let attributes: [Attribute]

  public init(string: String, attributes: [Attribute]) {
    self.string = string
    self.attributes = attributes
  }
}
