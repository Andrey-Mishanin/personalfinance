//
//  ID.swift
//  PersonalFinance
//
//  Created by Andrey Mishanin on 31/10/2020.
//  Copyright © 2020 Andrey Mishanin. All rights reserved.
//

import Foundation

public struct ID: Equatable {
  public enum Source {
    case saver
    case koku
    case personalFinance
  }

  public let value: Int
  public let source: Source

  public init(value: Int, source: Source) {
    self.value = value
    self.source = source
  }
}
