//
//  TableUIObserver.swift
//  PersonalFinance
//
//  Created by Andrey Mishanin on 24/04/2017.
//  Copyright © 2017 Andrey Mishanin. All rights reserved.
//

import Foundation

public protocol TableUIObserver: class {
  func uiWillAppear()
  func rowWasSelected(at indexPath: IndexPath)
  func row(at index: Int, didChangeDetailTextTo detailText: String)
}
