//
//  SequenceExtensions.swift
//  PersonalFinance
//
//  Created by Andrey Mishanin on 29/08/2018.
//  Copyright © 2018 Andrey Mishanin. All rights reserved.
//

import Foundation

extension Sequence {
  func count(where predicate: (Element) throws -> Bool) rethrows -> Int {
    var count = 0
    for element in self {
      if try predicate(element) {
        count += 1
      }
    }
    return count
  }
}
