//
//  Currency.swift
//  PersonalFinance
//
//  Created by Andrey Mishanin on 23/03/2015.
//  Copyright (c) 2015 Andrey Mishanin. All rights reserved.
//

public struct Currency {
  public let name: String
  public let symbol: String

  public static let britishPound = Currency(name: "British Pound", symbol: "£")
  public static let russianRouble = Currency(name: "Russian Rouble", symbol: "₽")
  public static let usDollar = Currency(name: "US Dollar", symbol: "$")
  public static let euro = Currency(name: "Euro", symbol: "€")
}
