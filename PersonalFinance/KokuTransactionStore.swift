//
//  KokuTransactionStore.swift
//  PersonalFinance
//
//  Created by Andrey Mishanin on 17/10/2020.
//  Copyright © 2020 Andrey Mishanin. All rights reserved.
//

import Foundation

public class KokuTransactionStore: TransactionStoreProtocol {
  public var nextTransactionID: ID {
    ID(value: 0, source: .personalFinance)
  }

  public unowned var observer: TransactionStoreObserver!

  public init() { }

  public func loadAllTransactions() {
    DispatchQueue.global().async {
      let url = Bundle.main.url(forResource: "database", withExtension: "sqlite")!
      let db = openDatabase(url: url)!
      let query = "SELECT ZFRTRANSACTION.Z_PK, ZACCOUNT, ZFRACCOUNT.ZNAME, ZDATE, ZDEPOSIT, ZWITHDRAWAL, ZFRTAG.Z_PK, ZFRTAG.ZNAME, ZINFO, ZNOTE FROM ZFRTRANSACTION JOIN ZFRACCOUNT ON ZFRTRANSACTION.ZACCOUNT = ZFRACCOUNT.Z_PK JOIN Z_11TRANSACTIONS ON ZFRTRANSACTION.Z_PK = Z_12TRANSACTIONS1 JOIN ZFRTAG ON Z_11TAGS = ZFRTAG.Z_PK"
      let kokuTransactions: [KokuTransaction]! = readRows(from: db, query: query)
      let transactions = kokuTransactions.map(Transaction.init(kokuTransaction:))
      DispatchQueue.main.async {
        self.observer.transactionStoreDidLoad(transactions: transactions)
      }
    }
  }

  public func update(transaction _: Transaction) {
    fatalError("Koku transactions should never be updated")
  }
}
