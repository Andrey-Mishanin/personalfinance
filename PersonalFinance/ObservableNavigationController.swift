//
//  ObservableNavigationController.swift
//  PersonalFinance
//
//  Created by Andrey Mishanin on 30/04/2018.
//  Copyright © 2018 Andrey Mishanin. All rights reserved.
//

import UIKit

protocol NavigationControllerObserver: class {
  func didPop(viewController: UIViewController)
}

final class ObservableNavigationController: UINavigationController {
  var observer: NavigationControllerObserver?

  override func popViewController(animated: Bool) -> UIViewController? {
    let prevVC = super.popViewController(animated: animated)
    if let vc = prevVC {
      observer?.didPop(viewController: vc)
    }
    return prevVC
  }
}
