//
//  UIRoute.swift
//  PersonalFinance
//
//  Created by Andrey Mishanin on 09/03/2015.
//  Copyright (c) 2015 Andrey Mishanin. All rights reserved.
//

import Foundation

public enum Screen {
  case addTransactionQuickly
  case accounts
}

public final class UIRoute {
  public init() {
  }

  public func startScreenForAccounts(_ accounts: [Account]) -> Screen {
    return .addTransactionQuickly
  }
}
