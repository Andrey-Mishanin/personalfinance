//
//  Account.swift
//  PersonalFinance
//
//  Created by Andrey Mishanin on 06/03/2015.
//  Copyright (c) 2015 Andrey Mishanin. All rights reserved.
//

import Foundation

public struct Account {
  public let id: Int
  public let name: String
  public let type: AccountType
  public let currency: Currency

  public init(id: Int, name: String, type: AccountType, currency: Currency) {
    self.id = id
    self.name = name
    self.type = type
    self.currency = currency
  }
}

extension Account: Equatable {
  public static func ==(lhs: Account, rhs: Account) -> Bool {
    return lhs.id == rhs.id
  }
}

extension Account: Named { }
