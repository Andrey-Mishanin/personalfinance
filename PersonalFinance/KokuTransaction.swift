//
//  KokuTransaction.swift
//  PersonalFinance
//
//  Created by Andrey Mishanin on 11/10/2020.
//  Copyright © 2020 Andrey Mishanin. All rights reserved.
//

import Foundation

public struct KokuTransaction: Decodable {
  enum CodingKeys: Int, CodingKey {
    case primaryKey = 0
    case accountPrimaryKey
    case accountName
    case dateTimestamp
    case deposit
    case withdrawal
    case categoryPrimaryKey
    case categoryName
    case info
    case note
  }

  let primaryKey: Int
  let accountPrimaryKey: Int
  let accountName: String
  let dateTimestamp: TimeInterval
  let deposit: Double
  let withdrawal: Double
  let categoryPrimaryKey: Int
  let categoryName: String
  let info: String
  let note: String?

  var date: Date {
    Date(timeIntervalSinceReferenceDate: dateTimestamp)
  }
}

extension KokuTransaction: CustomDebugStringConvertible {
  public var debugDescription: String {
    "\(primaryKey)|\(accountPrimaryKey)|\(accountName)|\(deposit)|\(date)|\(withdrawal)|\(categoryPrimaryKey)|\(categoryName)|\(info)|\(note ?? "")"
  }
}

extension KokuTransaction {
  var accountType: AccountType {
    switch accountPrimaryKey {
    case 2, 4:
      return .creditCard
    case 3:
      return .cash
    case 5...7:
      return .savings
    default:
      return .cash
    }
  }

  var accountCurrency: Currency {
    switch accountPrimaryKey {
    case 6:
      return .euro
    case 7:
      return .usDollar
    default:
      return .russianRouble
    }
  }
}

extension Transaction {
  init(kokuTransaction: KokuTransaction) {
    id = ID(value: kokuTransaction.primaryKey, source: .koku)
    let amount: Decimal
    if kokuTransaction.withdrawal > 0 {
      amount = -Decimal(kokuTransaction.withdrawal)
    } else {
      amount = Decimal(kokuTransaction.deposit)
    }
    self.amount = amount
    date = kokuTransaction.date
    category = TransactionCategory(
      id: kokuTransaction.categoryPrimaryKey,
      name: kokuTransaction.categoryName
    )
    info = kokuTransaction.info
    account = Account(
      id: kokuTransaction.accountPrimaryKey,
      name: kokuTransaction.accountName,
      type: kokuTransaction.accountType,
      currency: kokuTransaction.accountCurrency)
  }
}
