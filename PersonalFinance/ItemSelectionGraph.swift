//
//  ItemSelectionGraph.swift
//  PersonalFinance
//
//  Created by Andrey Mishanin on 27/02/2018.
//  Copyright © 2018 Andrey Mishanin. All rights reserved.
//

import Foundation
import PersonalFinanceCore_iOS
import UIKit

final class ItemSelectionGraph<Item> where Item: Equatable, Item: Named {
  let controller: ListSelectionController<Item>
  var vc: UIViewController & TableUI { return vcGraph.tableViewController }
  private let vcGraph: TableViewControllerGraph

  init(model: ListSelectionController<Item>.Model, title: String) {
    controller = ListSelectionController(model: model)
    vcGraph = TableViewControllerGraph(observer: controller)

    controller.ui = vc
    vc.navigationItem.title = title
  }
}
