//
//  AccountType.swift
//  PersonalFinance
//
//  Created by Andrey Mishanin on 23/03/2015.
//  Copyright (c) 2015 Andrey Mishanin. All rights reserved.
//

public enum AccountType {
  case creditCard
  case cash
  case savings
}
