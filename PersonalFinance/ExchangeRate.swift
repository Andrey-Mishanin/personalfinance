//
//  ExchangeRate.swift
//  PersonalFinance
//
//  Created by Andrey Mishanin on 06/06/2015.
//  Copyright (c) 2015 Andrey Mishanin. All rights reserved.
//

import Foundation

public struct ExchangeRate {
  public let fromCurrency: Currency
  public let toCurrency: Currency
  public let rate: Double
}
