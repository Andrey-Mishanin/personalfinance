//
//  ListSelectionController.swift
//  PersonalFinance
//
//  Created by Andrey Mishanin on 25/12/2017.
//  Copyright © 2017 Andrey Mishanin. All rights reserved.
//

import Foundation

public protocol Named {
  var name: String { get }
}

public final class ListSelectionController<Item> where Item: Equatable, Item: Named {
  public struct Model {
    public var items: [Item]
    public var selectedIdx: Int?

    public init(items: [Item], selectedItem: Item?) {
      let idx = selectedItem.flatMap { items.firstIndex(of: $0) }
      self.init(items: items, selectedIdx: idx)
    }

    init(items: [Item], selectedIdx: Int?) {
      precondition(selectedIdx == nil || items.indices.contains(selectedIdx!))
      self.items = items
      self.selectedIdx = selectedIdx
    }

    var selectedItem: Item? { return selectedIdx.map { items[$0] } }
  }

  public weak var ui: TableUI!
  private var model: Model {
    didSet {
      updateVM()
    }
  }

  public var selectedItem: Item? { return model.selectedItem }

  public init(model: Model) {
    self.model = model
  }

  private func updateVM() {
    let vm = TableViewModel(sections: [
      TableViewModel.Section(rows: model.items.enumerated().map {
        TableViewModel.Row(title: $0.1.name.withAttributes([]),
                           detail: "".withAttributes([]),
                           style: .plain,
                           selectionStyle: .gray,
                           accessoryType: $0.0 == model.selectedIdx ? .checkmark : .none,
                           id: $0.0)
      }, id: 0)
    ])

    ui.model = vm
  }
}

extension ListSelectionController: TableUIObserver {
  public func uiWillAppear() {
    updateVM()
  }

  public func rowWasSelected(at indexPath: IndexPath) {
    model.selectedIdx = indexPath.item
  }

  public func row(at index: Int, didChangeDetailTextTo detailText: String) {
  }
}
