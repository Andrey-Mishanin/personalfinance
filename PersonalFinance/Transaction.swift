//
//  Transaction.swift
//  PersonalFinance
//
//  Created by Andrey Mishanin on 05/01/2018.
//  Copyright © 2018 Andrey Mishanin. All rights reserved.
//

import Foundation

public struct Transaction {
  public let id: ID
  public var amount: Decimal
  public var date: Date
  public var category: TransactionCategory
  public var info: String
  public var account: Account

  public init(id: ID,
              amount: Decimal,
              date: Date,
              category: TransactionCategory,
              info: String = "",
              account: Account) {
    self.id = id
    self.amount = amount
    self.date = date
    self.category = category
    self.info = info
    self.account = account
  }
}

extension Transaction: Equatable {
  public static func ==(lhs: Transaction, rhs: Transaction) -> Bool {
    return lhs.id == rhs.id &&
      lhs.amount == rhs.amount &&
      lhs.category == rhs.category &&
      lhs.info == rhs.info &&
      lhs.account == rhs.account
  }
}
