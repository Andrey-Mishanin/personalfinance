//
//  AppDelegate.swift
//  PersonalFinance
//
//  Created by Andrey Mishanin on 21/09/2014.
//  Copyright (c) 2014 Andrey Mishanin. All rights reserved.
//

import UIKit
import PersonalFinanceCore_iOS

@UIApplicationMain
final class AppDelegate: UIResponder, UIApplicationDelegate {
  var window: UIWindow? = UIWindow(frame: UIScreen.main.bounds)

  private let wireframe = Wireframe()

  func application(_ application: UIApplication, didFinishLaunchingWithOptions _: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
    window!.rootViewController = wireframe.makeRootVC()
    window!.makeKeyAndVisible()
    return true
  }
}

