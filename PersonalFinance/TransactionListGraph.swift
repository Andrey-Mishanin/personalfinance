//
//  TransactionListGraph.swift
//  PersonalFinance
//
//  Created by Andrey Mishanin on 24/02/2018.
//  Copyright © 2018 Andrey Mishanin. All rights reserved.
//

import Foundation
import PersonalFinanceCore_iOS
import UIKit
import SwiftUI

final class TransactionListGraph<Store: TransactionStoreProtocol> {
  let controller: TransactionListController<Store>
  let vc: UIViewController
  private let observableTableUI: ObservableTableUI

  init(store: Store, wireframe: WireframeProtocol) {
    controller = TransactionListController(store: store, wireframe: wireframe, colorForCategory: color(for:))
    store.observer = controller
    observableTableUI = ObservableTableUI(observer: controller)
    controller.ui = observableTableUI
    vc = UIHostingController(rootView: DynamicTableView(observable: observableTableUI))
    vc.tabBarItem = UITabBarItem(title: "Transactions", image: UIImage(systemName: "list.bullet"), selectedImage: nil)
  }
}

func color(for category: TransactionCategory) -> PersonalFinanceCore_iOS.Color {
  return .clear
//  let colors: [Int : Color] = [
//    0 : .systemGray,
//    1 : .systemBlue,
//    2 : .systemOrange,
//    3 : .systemPurple,
//    4 : .systemRed,
//    5 : .systemGreen,
//    6 : .purple,
//    7 : .systemPink,
//    8 : .brown,
//    9 : .systemYellow,
//    10 : .systemGray2,
//    11 : .systemBlue,
//    12 : .systemPurple,
//    13 : .systemGreen,
//    14 : .systemTeal
//    ]
//  return colors[category.id]!
}
