//
//  AccountListVC.swift
//  PersonalFinance
//
//  Created by Andrey Mishanin on 09/03/2015.
//  Copyright (c) 2015 Andrey Mishanin. All rights reserved.
//

import UIKit
import PersonalFinanceCore_iOS

final class AccountListVC: UIViewController {
  lazy var accountCreationPromptTextView: UILabel = {
    let textView = UILabel()
    textView.font = UIFont.systemFont(ofSize: 18, weight: UIFont.Weight.light)
    textView.numberOfLines = 3
    return textView
  }()

  let model: AccountsUIModel

  init(model: AccountsUIModel) {
    self.model = model
    super.init(nibName: nil, bundle: nil)
  }

  required init?(coder _: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }

  override func viewDidLoad() {
    view.backgroundColor = .white
    view.addSubview(accountCreationPromptTextView)

    let policy = AccountsUIPolicy()
    let action = policy.decideActionForAccounts(model.accounts)
    switch (action) {
    case .displayAccountCreationPrompt:
      accountCreationPromptTextView.text = model.accountCreationPrompt
    case .displayAccountList:
      view.backgroundColor = .red
    }
  }

  override func viewWillLayoutSubviews() {
    let promptSize = accountCreationPromptTextView.sizeThatFits(CGSize(width: view.bounds.width - 40, height: CGFloat.greatestFiniteMagnitude))
    accountCreationPromptTextView.frame.size = promptSize
    accountCreationPromptTextView.center = CGPoint(x: view.bounds.midX, y: view.bounds.midY)
  }
}
