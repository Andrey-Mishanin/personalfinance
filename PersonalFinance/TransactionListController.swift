//
//  TransactionListController.swift
//  PersonalFinance
//
//  Created by Andrey Mishanin on 06/01/2018.
//  Copyright © 2018 Andrey Mishanin. All rights reserved.
//

import Foundation

public final class TransactionListController<Store: TransactionStoreProtocol> {
  private unowned let store: Store
  private unowned let wireframe: WireframeProtocol
  private let colorForCategory: (TransactionCategory) -> Color
  public weak var ui: TableUI!
  private var transactionsByMonth: [(month: DateComponents, transactions: [Transaction])] = [] {
    didSet {
      updateVM()
    }
  }

  public init(store: Store, wireframe: WireframeProtocol, colorForCategory: @escaping (TransactionCategory) -> Color) {
    self.store = store
    self.wireframe = wireframe
    self.colorForCategory = colorForCategory
  }
}

extension TransactionListController: TableUIObserver {
  public func uiWillAppear() {
    store.loadAllTransactions()
  }

  private func updateVM() {
    let sections = transactionsByMonth.enumerated().map {
      return TableViewModel.Section(
        name: sectionNameFormatter.string(from: Calendar.current.date(from: $0.1.month)!),
        rows: $0.1.transactions.enumerated().map(row(from:)),
        id: $0.0
      )
    }
    let vm = TableViewModel(sections: sections)
    ui.model = vm
  }

  private func row(from pair: (Int, Transaction)) -> TableViewModel.Row {
    let transaction = pair.1;
    let formatter = makeAmountFormatter(currencySymbol: transaction.account.currency.symbol)
    let amountStr = formatter.string(from: transaction.amount as NSDecimalNumber)!
    let style: TableViewModel.Row.Style = .form(editableStyle: .none,
                                                isFirstResponder: false,
                                                badgeColor: colorForCategory(transaction.category))
    return TableViewModel.Row(title: transaction.info.withAttributes([]),
                              detail: amountStr.withAttributes(attributes(for: transaction.amount)),
                              style: style,
                              selectionStyle: .gray,
                              accessoryType: .disclosureIndicator,
                              id: pair.0)
  }

  public func rowWasSelected(at indexPath: IndexPath) {
    wireframe.presentEditTransactionUI(transaction: transactionsByMonth[indexPath.section].transactions[indexPath.item],
                                       completion: { [unowned self] transaction in
                                        store.update(transaction: transaction)
                                       })
  }

  public func row(at index: Int, didChangeDetailTextTo detailText: String) { }
}

extension TransactionListController: TransactionStoreObserver {
  public func transactionStoreDidLoad(transactions: [Transaction]) {
    transactionsByMonth = Dictionary(
      grouping: transactions,
      by: { Calendar.current.dateComponents([.month, .year], from: $0.date) }
    ).sorted {
      Calendar.current.date(from: $0.key)! > Calendar.current.date(from: $1.key)!
    }.map {
      ($0.key, $0.value.sorted(by: { $0.date > $1.date }))
    }
  }
}

private func attributes(for amount: Decimal) -> [AttributedString.Attribute] {
  [
    .font(.boldSystemFont(ofSize: 17)),
    .paragraphStyle(.right)
  ] + (amount > 0 ? [.textColor(.systemGreen)] : [])
}

private func makeAmountFormatter(currencySymbol: String) -> NumberFormatter {
  let formatter = NumberFormatter()
  formatter.numberStyle = .currency
  formatter.currencySymbol = currencySymbol
  formatter.minusSign = ""
  formatter.positivePrefix = formatter.plusSign + formatter.currencySymbol
  return formatter
}

private let sectionNameFormatter: DateFormatter = {
  let formatter = DateFormatter()
  formatter.timeStyle = .none
  formatter.dateFormat = DateFormatter.dateFormat(fromTemplate: "MMMM yyyy", options: 0, locale: .current)
  return formatter
}()
