//
//  EditTransactionController.swift
//  PersonalFinance
//
//  Created by Andrey Mishanin on 21/04/2017.
//  Copyright © 2017 Andrey Mishanin. All rights reserved.
//

import Foundation

public final class EditTransactionController {
  public enum Params {
    case newTransaction(provisionalID: ID)
    case existingTransaction(transaction: Transaction)
  }

  private enum Row: Int {
    case amount
    case date
    case category
    case account
    case info
  }

  public weak var ui: TableUI!
  public let setCommitEnabled: (Bool) -> Void
  private unowned let wireframe: WireframeProtocol
  private let ID: ID

  private var amount: Decimal?
  private var selectedCategory: TransactionCategory? {
    didSet {
      updateUI()
    }
  }
  private var account: Account? {
    didSet {
      updateUI()
    }
  }
  private var info = ""
  private var date: Date?

  public init(params: Params, wireframe: WireframeProtocol, setCommitEnabled: @escaping (Bool) -> Void) {
    switch params {
    case let .newTransaction(provisionalID):
      ID = provisionalID
    case let .existingTransaction(transaction):
      ID = transaction.id
      amount = transaction.amount
      selectedCategory = transaction.category
      info = transaction.info
      account = transaction.account
      date = transaction.date
    }
    self.wireframe = wireframe
    self.setCommitEnabled = setCommitEnabled
  }

  private func updateUI() {
    let amountDetailText = (amount.flatMap { currencyFormatter.string(from: $0 as NSDecimalNumber) } ?? "").withAttributes(leftAlignedAttrs)
    let vm = TableViewModel(sections: [
      TableViewModel.Section(rows: [
        TableViewModel.Row(title: NSLocalizedString("Amount", comment: "").withAttributes(rightAlignedAttrs),
                           detail: amountDetailText,
                           style: .form(editableStyle: .currency, isFirstResponder: true, badgeColor: nil),
                           id: 0),
        TableViewModel.Row(title: NSLocalizedString("Date", comment: "").withAttributes(rightAlignedAttrs),
                           detail: dateFormatter.string(from: date ?? Date()).withAttributes(leftAlignedAttrs),
                           style: .form(editableStyle: .none, isFirstResponder: false, badgeColor: nil),
                           id: 1),
        TableViewModel.Row(title: NSLocalizedString("Category", comment: "").withAttributes(rightAlignedAttrs),
                           detail: (selectedCategory?.name ?? "").withAttributes(leftAlignedAttrs),
                           style: .form(editableStyle: .none, isFirstResponder: false, badgeColor: nil),
                           selectionStyle: .gray,
                           accessoryType: .disclosureIndicator,
                           id: 2),
        TableViewModel.Row(title: NSLocalizedString("Account", comment: "").withAttributes(rightAlignedAttrs),
                           detail: (account?.name ?? "default").withAttributes(leftAlignedAttrs),
                           style: .form(editableStyle: .none, isFirstResponder: false, badgeColor: nil),
                           selectionStyle: .gray,
                           accessoryType: .disclosureIndicator,
                           id: 3),
        TableViewModel.Row(title: NSLocalizedString("Info", comment: "").withAttributes(rightAlignedAttrs),
                           detail: info.withAttributes(leftAlignedAttrs),
                           style: .form(editableStyle: .text, isFirstResponder: false, badgeColor: nil),
                           id: 4),

      ], id: 0)
    ])

    ui.model = vm
    setCommitEnabled(transaction != nil)
  }

  public var transaction: Transaction? {
    guard let amount = amount, let category = selectedCategory, let account = account else {
      return nil
    }
    return Transaction(id: ID,
                       amount: amount,
                       date: date ?? Date(),
                       category: category,
                       info: info,
                       account: account)
  }
}

extension EditTransactionController: TableUIObserver {
  public func uiWillAppear() {
    updateUI()
  }

  public func rowWasSelected(at indexPath: IndexPath) {
    let row = Row(rawValue: indexPath.item)!

    switch row {
    case .category:
      wireframe.presentCategorySelectionUI(withSelectedCategory: selectedCategory,
                                           completion: { [unowned self] in self.selectedCategory = $0 })
    case .account:
      wireframe.presentAccountSelectionUI(withSelectedAccount: account,
                                          completion: { [unowned self] in self.account = $0 })
    case .amount, .date, .info:
      break
    }
  }

  public func row(at index: Int, didChangeDetailTextTo detailText: String) {
    let row = Row(rawValue: index)!

    switch row {
    case .amount:
      amount = currencyFormatter.number(from: detailText)?.decimalValue
      setCommitEnabled(transaction != nil)
    case .info:
      info = detailText
    case .date, .category, .account:
      break
    }
  }
}

private let leftAlignedAttrs: [AttributedString.Attribute] = [
  .font(.systemFont(ofSize: 17)),
  .paragraphStyle(.left),
  .textColor(.gray),
  .baselineOffset(-1),
]

private let rightAlignedAttrs: [AttributedString.Attribute] = [
  .font(.boldSystemFont(ofSize: 17)),
  .paragraphStyle(.right)
]

private let dateFormatter: DateFormatter = {
  let formatter = DateFormatter()
  formatter.dateStyle = .short
  formatter.timeStyle = .none
  return formatter
}()

private let currencyFormatter: NumberFormatter = {
  let formatter = NumberFormatter()
  formatter.maximumFractionDigits = 2
  formatter.minimumFractionDigits = 2
  formatter.minusSign = ""
  return formatter
}()
