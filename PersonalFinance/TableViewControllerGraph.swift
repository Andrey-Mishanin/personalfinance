//
//  TableViewControllerGraph.swift
//  PersonalFinance
//
//  Created by Andrey Mishanin on 24/02/2018.
//  Copyright © 2018 Andrey Mishanin. All rights reserved.
//

import Foundation
import UIKit
import PersonalFinanceCore_iOS

final class TableViewControllerGraph {
  private unowned let observer: TableUIObserver
  private let dataSource: TableDataSource
  private let delegate: TableDelegate
  private let tableView: UITableView
  let tableViewController: TableViewController

  init(observer: TableUIObserver) {
    self.observer = observer
    tableView = UITableView(frame: .zero, style: .plain)
    tableView.register(TableViewCell.self, forCellReuseIdentifier: TableViewCell.reuseId)
    tableView.keyboardDismissMode = .interactive
    dataSource = TableDataSource(observer: observer)
    tableView.dataSource = dataSource
    delegate = TableDelegate(observer: observer)
    tableView.delegate = delegate
    tableViewController = TableViewController(observer: observer, tableView: tableView, dataSource: dataSource)
  }
}
