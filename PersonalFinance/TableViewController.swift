//
//  TableViewController.swift
//  PersonalFinance
//
//  Created by Andrey Mishanin on 06/04/2017.
//  Copyright © 2017 Andrey Mishanin. All rights reserved.
//

import UIKit
import PersonalFinanceCore_iOS

final class TableViewController: UIViewController, TableUI {
  var model: TableViewModel {
    get {
      return dataSource.model
    }
    set {
      dataSource.model = newValue
      tableView.reloadData()
    }
  }

  private unowned let observer: TableUIObserver
  private unowned let dataSource: TableDataSource
  private unowned let tableView: UITableView
  private let statusBarBackground: UIView

  init(observer: TableUIObserver, tableView: UITableView, dataSource: TableDataSource) {
    self.observer = observer
    self.tableView = tableView
    self.dataSource = dataSource
    statusBarBackground = UIVisualEffectView(effect: UIBlurEffect(style: .light))
    super.init(nibName: nil, bundle: nil)
  }

  required init?(coder aDecoder: NSCoder) {
    fatalError("not implemented")
  }

  override func viewDidLoad() {
    view.backgroundColor = .white
    view.addSubview(tableView)
    view.addSubview(statusBarBackground)
  }

  override func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(animated)
    observer.uiWillAppear()
  }

  override func viewDidAppear(_ animated: Bool) {
    super.viewDidAppear(animated)

    if let indexPath = model.indexPathOfFirstResponderRow {
      let cell = tableView.cellForRow(at: indexPath) as! TableViewCell
      let _ = cell.becomeFirstResponder()
    }
  }

  override func viewWillLayoutSubviews() {
    tableView.frame = view.bounds
    let statusBarFrame = view.window?.windowScene?.statusBarManager?.statusBarFrame ?? .zero
    let statusBarFrameInView = view.window?.convert(statusBarFrame, to: view) ?? .zero
    let statusBarOverlap = statusBarFrameInView.intersection(view.bounds).height
    statusBarBackground.frame = CGRect(origin: .zero,
                                       size: CGSize(width: view.bounds.width, height: statusBarOverlap))
  }
}

final class TableDataSource: NSObject, UITableViewDataSource {
  fileprivate var model = TableViewModel(sections: [TableViewModel.Section(rows: [], id: 0)])
  private unowned let observer: TableUIObserver

  init(observer: TableUIObserver) {
    self.observer = observer
  }

  func numberOfSections(in tableView: UITableView) -> Int {
    model.sections.count
  }

  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return model.sections[section].rows.count
  }

  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    let cell = tableView.dequeueReusableCell(withIdentifier: TableViewCell.reuseId, for: indexPath) as! TableViewCell
    cell.row = model.sections[indexPath.section].rows[indexPath.row]
    cell.textFieldDelegate = TextFieldDelegate(index: indexPath.row, observer: observer)
    return cell
  }

  func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
    model.sections[section].name
  }
}

final class TextFieldDelegate: NSObject, UITextFieldDelegate {
  private let index: Int
  private unowned let observer: TableUIObserver

  init(index: Int, observer: TableUIObserver) {
    self.index = index
    self.observer = observer
  }

  func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
    let currentText = textField.text ?? ""
    let replacementRange = Range(range, in: currentText)!
    let finalText = currentText.replacingCharacters(in: replacementRange, with: string)
    observer.row(at: index, didChangeDetailTextTo: finalText)
    return true
  }
}

final class TableDelegate: NSObject, UITableViewDelegate {
  private unowned let observer: TableUIObserver

  init(observer: TableUIObserver) {
    self.observer = observer
  }

  func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    observer.rowWasSelected(at: indexPath)
  }
}

final class TableViewCell: UITableViewCell {
  static let reuseId = "TableViewCell"

  var row: TableViewModel.Row! {
    didSet {
      updateView()
      setNeedsLayout()
    }
  }
  var textFieldDelegate: UITextFieldDelegate? {
    didSet {
      detailTextField.delegate = textFieldDelegate
    }
  }

  private var detailLabel: UILabel
  private var detailTextField: UITextField
  private var badge: UIView

  override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
    detailLabel = UILabel()
    detailTextField = UITextField()
    badge = UIView()
    super.init(style: .default, reuseIdentifier: TableViewCell.reuseId)
    contentView.addSubview(detailLabel)
    contentView.addSubview(detailTextField)
    contentView.addSubview(badge)
  }
  
  required init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }

  override var canBecomeFirstResponder: Bool {
    return true
  }

  override func becomeFirstResponder() -> Bool {
    detailTextField.becomeFirstResponder()
    return true
  }

  private func updateView() {
    textLabel?.attributedText = row.title.toCocoa
    selectionStyle = row.selectionStyle.toCocoa
    switch row.style {
    case .plain:
      accessoryType = row.accessoryType.toCocoa
    case let .form(editableStyle, _, badgeColor):
      if editableStyle.isEditable {
        detailTextField.attributedText = row.detail.toCocoa
        detailTextField.defaultTextAttributes =
          convertToNSAttributedStringKeyDictionary(convertAttributesToCocoa(row.detail.attributes).mapKeys { $0.rawValue })
        detailTextField.keyboardType = editableStyle.keyboardType
        accessoryType = .none
      } else {
        detailLabel.attributedText = row.detail.toCocoa
        accessoryType = row.accessoryType.toCocoa
      }
      badge.backgroundColor = badgeColor
    }
  }

  override func layoutSubviews() {
    super.layoutSubviews()
    let gapBetweenLabels: CGFloat = 10
    let layoutBounds = contentView.bounds
    let midX = convert(bounds.center, to: contentView).x
    let textFrame = CGRect(origin: CGPoint(x: separatorInset.left, y: 0),
                           size: CGSize(width: midX - gapBetweenLabels / 2 - separatorInset.left,
                                        height: layoutBounds.height))
    textLabel?.frame = textFrame

    let detailOrigin = CGPoint(x: textFrame.maxX + gapBetweenLabels, y: 0)
    let detailFrame = CGRect(origin: detailOrigin,
                             size: CGSize(width: layoutBounds.width - detailOrigin.x,
                                          height: layoutBounds.height))
    switch row.style {
    case .plain:
      break
    case let .form(editableStyle, _, badgeColor):
      if editableStyle.isEditable {
        detailTextField.frame = detailFrame
        detailLabel.frame = .zero
      } else {
        detailTextField.frame = .zero
        detailLabel.frame = detailFrame
      }
      badge.frame = badgeColor != nil ? CGRect(x: 0, y: 0, width: 10, height: layoutBounds.height) : .zero
    }
  }
}

private extension CGRect {
  var center: CGPoint { return CGPoint(x: midX, y: midY) }
}

private extension EditableStyle {
  var keyboardType: UIKeyboardType {
    switch self {
    case .currency:
      return .decimalPad
    case .date:
      return .numbersAndPunctuation
    case .text:
      return .default
    default:
      fatalError()
    }
  }
}

private extension TableViewModel.Row.SelectionStyle {
  var toCocoa: UITableViewCell.SelectionStyle {
    switch self {
    case .none:
      return .none
    case .gray:
      return .gray
    }
  }
}

private extension TableViewModel.Row.AccessoryType {
  var toCocoa: UITableViewCell.AccessoryType {
    switch self {
    case .none:
      return .none
    case .checkmark:
      return .checkmark
    case .disclosureIndicator:
      return .disclosureIndicator
    }
  }
}

// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertToNSAttributedStringKeyDictionary(_ input: [String: Any]) -> [NSAttributedString.Key: Any] {
	return Dictionary(uniqueKeysWithValues: input.map { key, value in (NSAttributedString.Key(rawValue: key), value)})
}
