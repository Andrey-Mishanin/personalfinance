//
//  SaverTransactionStore.swift
//  PersonalFinance
//
//  Created by Andrey Mishanin on 04/10/2020.
//  Copyright © 2020 Andrey Mishanin. All rights reserved.
//

import Foundation

public class SaverTransactionStore: TransactionStoreProtocol {
  public var nextTransactionID: ID {
    ID(value: 0, source: .personalFinance)
  }

  public unowned var observer: TransactionStoreObserver!

  public init() { }

  public func loadAllTransactions() {
    DispatchQueue.global().async {
      let url = Bundle.main.url(forResource: "calculon", withExtension: "sqlite3")!
      let db = openDatabase(url: url)!
      let query = "SELECT * FROM expences"
      let saverTransactions: [SaverTransaction]! = readRows(from: db, query: query)
      let transactions = saverTransactions.map(Transaction.init(saverTransaction:))
      DispatchQueue.main.async {
        self.observer.transactionStoreDidLoad(transactions: transactions)
      }
    }
  }

  public func update(transaction _: Transaction) {
    fatalError("Saver transactions should never be updated")
  }
}
