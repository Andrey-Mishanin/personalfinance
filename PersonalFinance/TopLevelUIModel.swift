//
//  TopLevelUIModel.swift
//  PersonalFinance
//
//  Created by Andrey Mishanin on 23/03/2015.
//  Copyright (c) 2015 Andrey Mishanin. All rights reserved.
//

public protocol TopLevelUIModel {
  var title: String { get }
  var screen: Screen { get }
}
