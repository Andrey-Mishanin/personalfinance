//
//  AccountsUIPolicy.swift
//  PersonalFinance
//
//  Created by Andrey Mishanin on 06/03/2015.
//  Copyright (c) 2015 Andrey Mishanin. All rights reserved.
//

public enum AccountsUIPolicyAction {
  case displayAccountCreationPrompt
  case displayAccountList
}

public final class AccountsUIPolicy {
  public init() {
  }

  public func decideActionForAccounts(_ accounts: [Account]) -> AccountsUIPolicyAction {
    if accounts.isEmpty {
      return .displayAccountCreationPrompt
    } else {
      return .displayAccountList
    }
  }
}
