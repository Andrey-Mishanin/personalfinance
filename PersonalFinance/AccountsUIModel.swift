//
//  AccountsUIModel.swift
//  PersonalFinance
//
//  Created by Andrey Mishanin on 09/03/2015.
//  Copyright (c) 2015 Andrey Mishanin. All rights reserved.
//

import Foundation

public struct AccountsUIModel: TopLevelUIModel {
  public let accounts: [Account]

  public init(accounts: [Account]) {
    self.accounts = accounts
  }

  public var title: String {
    return "Accounts"
  }

  public var screen: Screen {
    return .accounts
  }

  public var accountCreationPrompt: String {
    return "You need at least one account to store transactions. Tap here to create one."
  }
}
