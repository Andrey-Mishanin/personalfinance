//
//  EditTransactionGraph.swift
//  PersonalFinance
//
//  Created by Andrey Mishanin on 25/02/2018.
//  Copyright © 2018 Andrey Mishanin. All rights reserved.
//

import Foundation
import UIKit
import PersonalFinanceCore_iOS

final class PresentationDelegate: NSObject, UIAdaptivePresentationControllerDelegate {
  private unowned let wireframe: WireframeProtocol

  init(wireframe: WireframeProtocol) {
    self.wireframe = wireframe
  }

  func presentationControllerDidDismiss(_: UIPresentationController) {
    wireframe.dismissTransactionUI(committing: false)
  }
}

final class EditTransactionGraph {
  let controller: EditTransactionController
  let navigationController: ObservableNavigationController
  private let vcGraph: TableViewControllerGraph
  private let presentationDelegate: PresentationDelegate
  private unowned let wireframe: WireframeProtocol

  init(params: EditTransactionController.Params,
       wireframe: WireframeProtocol & NavigationControllerObserver) {
    let doneItem = UIBarButtonItem(barButtonSystemItem: .done, target: nil, action: #selector(commitNewTransactionUI))
    controller = EditTransactionController(params: params, wireframe: wireframe, setCommitEnabled: {
      doneItem.isEnabled = $0
    })
    vcGraph = TableViewControllerGraph(observer: controller)
    self.wireframe = wireframe
    let title: String
    switch params {
    case .existingTransaction:
      title = "Edit transaction"
    case .newTransaction:
      title = "Add transaction"
    }
    let editTransactionVC = vcGraph.tableViewController
    editTransactionVC.navigationItem.title = NSLocalizedString(title, comment: "")
    controller.ui = editTransactionVC
    navigationController = ObservableNavigationController(rootViewController: editTransactionVC)
    navigationController.observer = wireframe

    presentationDelegate = PresentationDelegate(wireframe: wireframe)
    navigationController.presentationController?.delegate = presentationDelegate

    doneItem.target = self
    let cancelItem = UIBarButtonItem(barButtonSystemItem: .cancel,
                                     target: self,
                                     action: #selector(cancelNewTransactionUI))
    editTransactionVC.navigationItem.leftBarButtonItem = doneItem
    editTransactionVC.navigationItem.rightBarButtonItem = cancelItem
  }

  @objc func commitNewTransactionUI() {
    wireframe.dismissTransactionUI(committing: true)
  }

  @objc func cancelNewTransactionUI() {
    wireframe.dismissTransactionUI(committing: false)
  }
}
