//
//  AddTransactionQuicklyUIModel.swift
//  PersonalFinance
//
//  Created by Andrey Mishanin on 23/03/2015.
//  Copyright (c) 2015 Andrey Mishanin. All rights reserved.
//

public struct AddTransactionQuicklyUIModel: TopLevelUIModel {
  public init() {
  }

  public var title: String {
    return "+"
  }

  public var screen: Screen {
    return .addTransactionQuickly
  }
}
