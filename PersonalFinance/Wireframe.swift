//
//  Wireframe.swift
//  PersonalFinance
//
//  Created by Andrey Mishanin on 05/04/2017.
//  Copyright © 2017 Andrey Mishanin. All rights reserved.
//

import UIKit
import PersonalFinanceCore_iOS

final class Wireframe: WireframeProtocol {
  private enum State: Equatable {
    case initial
    case mainScreen(rootVC: UIViewController, graph: TransactionListGraph<KokuTransactionStore>)
    case newTransaction(graph: EditTransactionGraph, completion: (PersonalFinanceCore_iOS.Transaction?) -> Void)
    case categorySelection(navigationVC: UINavigationController, graph: ItemSelectionGraph<TransactionCategory>, completion: (TransactionCategory?) -> Void)
    case accountSelection(navigationVC: UINavigationController, graph: ItemSelectionGraph<Account>, completion: (Account?) -> Void)

    static func ==(lhs: State, rhs: State) -> Bool {
      switch (lhs, rhs) {
        case (.initial, .initial),
             (.mainScreen, .mainScreen),
             (.newTransaction, .newTransaction),
             (.categorySelection, .categorySelection),
             (.accountSelection, .accountSelection):
        return true
      default:
        return false
      }
    }
  }

  private var stateStack: [State] = [.initial]
  private let transactionStore = KokuTransactionStore()

  func makeRootVC() -> UIViewController {
    precondition(stateStack == [.initial], "You're supposed to create root VC only once")
    let rootVC = UITabBarController()
    let transactionListGraph = TransactionListGraph(store: transactionStore, wireframe: self)
    let transactionListVC = transactionListGraph.vc
    rootVC.viewControllers = [makeAddTransactionVC(), transactionListVC]
    stateStack.append(.mainScreen(rootVC: rootVC, graph: transactionListGraph))
    return rootVC
  }

  private func makeAddTransactionVC() -> UIViewController {
    let button = UIButton()
    button.setTitle("+", for: UIControl.State())
    button.setTitleColor(.label, for: UIControl.State())
    button.setTitleColor(.systemGray, for: .highlighted)
    button.titleLabel!.font = UIFont.systemFont(ofSize: 144)
    button.addTarget(self, action: #selector(presentNewTransactionUI), for: .touchUpInside)
    let vc = AddTransactionQuicklyVC(addButton: button)
    vc.tabBarItem = UITabBarItem(title: "Add", image: UIImage(systemName: "plus"), selectedImage: nil)
    return vc
  }

  func presentEditTransactionUI(transaction: PersonalFinanceCore_iOS.Transaction, completion: @escaping (PersonalFinanceCore_iOS.Transaction) -> Void) {
    switch stateStack.last! {
    case let .mainScreen(vc, _):
      presentEditTransactionUI(from: vc,
                               params: .existingTransaction(transaction: transaction),
                               completion: { editedTransaction in
                                if let t = editedTransaction { completion(t) }
      })
    default:
      fatalError("Can't \(#function) when in \(stateStack.last!)")
    }
  }

  @objc func presentNewTransactionUI() {
    switch stateStack.last! {
    case let .mainScreen(vc, _):
      presentEditTransactionUI(from: vc,
                               params: .newTransaction(provisionalID: transactionStore.nextTransactionID),
                               completion: { [unowned self] transaction in
                                if let t = transaction { transactionStore.update(transaction: t) }
      })
    default:
      fatalError("Can't \(#function) when in \(stateStack.last!)")
    }
  }

  private func presentEditTransactionUI(from vc: UIViewController,
                                        params: EditTransactionController.Params,
                                        completion: @escaping (PersonalFinanceCore_iOS.Transaction?) -> Void) {
    let graph = EditTransactionGraph(params: params, wireframe: self)
    stateStack.append(.newTransaction(graph: graph, completion: completion))
    vc.present(graph.navigationController, animated: true)
  }

  func dismissTransactionUI(committing: Bool) {
    let prevState = stateStack.popLast()!
    switch prevState {
    case let .newTransaction(graph, completion):
      if committing { completion(graph.controller.transaction) }
      graph.navigationController.dismiss(animated: true)
    default:
      fatalError("Can't \(#function) when in \(prevState)")
    }
  }

  func presentCategorySelectionUI(withSelectedCategory selectedCategory: TransactionCategory?,
                                  completion: @escaping (TransactionCategory?) -> Void) {
    let model = ListSelectionController.Model(items: saverCategories, selectedItem: selectedCategory)
    let graph = ItemSelectionGraph(model: model,
                                   title: NSLocalizedString("Select category", comment: ""))
    switch stateStack.last! {
    case let .newTransaction(prevGraph, _):
      prevGraph.navigationController.pushViewController(graph.vc, animated: true)
      stateStack.append(.categorySelection(navigationVC: prevGraph.navigationController,
                                           graph: graph,
                                           completion: completion))
    default:
      fatalError("Can't \(#function) when in \(stateStack.last!)")
    }
  }

  func presentAccountSelectionUI(withSelectedAccount selectedAccount: Account?,
                                 completion: @escaping (Account?) -> Void) {
    let model = ListSelectionController.Model(items: testAccounts,  selectedItem: selectedAccount)
    let graph = ItemSelectionGraph(model: model,
                                   title: NSLocalizedString("Select account", comment: ""))
    switch stateStack.last! {
    case let .newTransaction(prevGraph, _):
      prevGraph.navigationController.pushViewController(graph.vc, animated: true)
      stateStack.append(.accountSelection(navigationVC: prevGraph.navigationController, graph: graph, completion: completion))
    default:
      fatalError("Can't \(#function) when in \(stateStack.last!)")
    }
  }
}

extension Wireframe: NavigationControllerObserver {
  func didPop(viewController: UIViewController) {
    let prevState = stateStack.popLast()!
    switch prevState {
    case let .categorySelection(_, graph, completion) where graph.vc == viewController:
      completion(graph.controller.selectedItem)
    case let .accountSelection(_, graph, completion) where graph.vc == viewController:
      completion(graph.controller.selectedItem)
    default:
      fatalError("Can't \(#function) when in \(prevState)")
    }
  }
}

let testCategories = [
  "Health",
  "Hobby",
  "Hotel",
  "House",
  "Income"
].enumerated().map { TransactionCategory(id: $0, name: $1) }

let testAccounts = [
  ("Saver", .russianRouble),
  ("Альфа", .russianRouble),
  ("Наличные", .britishPound),
  ("Райффайзен", .russianRouble),
  ("Квартира", .russianRouble),
  ].enumerated().map { (arg: (Int, (String, Currency))) in
    return Account(id: arg.0, name: arg.1.0, type: .cash, currency: arg.1.1)
}
