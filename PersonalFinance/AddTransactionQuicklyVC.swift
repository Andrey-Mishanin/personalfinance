//
//  AddTransactionQuicklyVC.swift
//  PersonalFinance
//
//  Created by Andrey Mishanin on 23/03/2015.
//  Copyright (c) 2015 Andrey Mishanin. All rights reserved.
//

import UIKit
import PersonalFinanceCore_iOS

final class AddTransactionQuicklyVC: UIViewController {
  private let addButton: UIButton

  init(addButton: UIButton) {
    self.addButton = addButton
    super.init(nibName: nil, bundle: nil)
    title = NSLocalizedString("Add transaction", comment: "")
  }

  required init?(coder aDecoder: NSCoder) {
    fatalError("not implemented")
  }

  override func viewDidLoad() {
    view.backgroundColor = .systemBackground
    view.addSubview(addButton)
  }

  override func viewWillLayoutSubviews() {
    addButton.frame = view.bounds
  }
}
