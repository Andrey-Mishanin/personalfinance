//
//  TransactionStore.swift
//  PersonalFinance
//
//  Created by Andrey Mishanin on 04/10/2020.
//  Copyright © 2020 Andrey Mishanin. All rights reserved.
//

import Foundation

public protocol TransactionStoreProtocol: AnyObject {
  var observer: TransactionStoreObserver! { get set }

  var nextTransactionID: ID { get }
  func loadAllTransactions()
  func update(transaction: Transaction)
}

public protocol TransactionStoreObserver: AnyObject {
  func transactionStoreDidLoad(transactions: [Transaction])
}
