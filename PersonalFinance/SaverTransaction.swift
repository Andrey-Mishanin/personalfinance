//
//  SaverTransaction.swift
//  PersonalFinance
//
//  Created by Andrey Mishanin on 11/10/2020.
//  Copyright © 2020 Andrey Mishanin. All rights reserved.
//

import Foundation

public struct SaverTransaction: Decodable {
  enum CodingKeys: Int, CodingKey {
    case id = 0
    case category
    case amount
    case day
    case month
    case year
    case hour
    case minute
    case second
    case tag
    case entryNumber = 14
  }

  let id: Int
  let category: Int
  let amount: Double
  let day: Int
  let month: Int
  let year: Int
  let hour: Int
  let minute: Int
  let second: Int
  let tag: String
  let entryNumber: Int

  var date: Date {
    let dateComponents = DateComponents(calendar: Calendar.current, timeZone: TimeZone(identifier: "Europe/Moscow"), year: year, month: month, day: day, hour: hour, minute: minute, second: second)
    return dateComponents.date ?? Date()
  }
}

extension SaverTransaction: CustomDebugStringConvertible {
  public var debugDescription: String {
    "\(id)|\(category)|\(amount)|\(date)|\(tag)|\(entryNumber)"
  }
}

public extension Transaction {
  init(saverTransaction: SaverTransaction) {
    id = ID(value: saverTransaction.id, source: .saver)
    amount = -Decimal(saverTransaction.amount)
    date = saverTransaction.date
    category = saverCategories[saverTransaction.category]
    info = saverTransaction.tag
    account = Account(id: 0, name: "Saver", type: .cash, currency: .russianRouble)
  }
}

public let saverCategories: [TransactionCategory] = [
  .init(id: 0, name: "General"),
  .init(id: 1, name: "Kids"),
  .init(id: 2, name: "House"),
  .init(id: 3, name: "Amusement"),
  .init(id: 4, name: "Wardrobe"),
  .init(id: 5, name: "Groceries"),
  .init(id: 6, name: "Misc"),
  .init(id: 7, name: "Food"),
  .init(id: 8, name: "Payments"),
  .init(id: 9, name: "Transport"),
  .init(id: 10, name: "Utilities"),
  .init(id: 11, name: "Personal"),
  .init(id: 12, name: "iTunes"),
  .init(id: 13, name: "Auto"),
  .init(id: 14, name: "Vacation"),
]
