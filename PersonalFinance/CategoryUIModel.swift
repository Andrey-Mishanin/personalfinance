//
//  CategoryUIModel.swift
//  PersonalFinance
//
//  Created by Andrey Mishanin on 06/06/2015.
//  Copyright (c) 2015 Andrey Mishanin. All rights reserved.
//

import Foundation

public struct CategoryUIModel {
  public let categoryName: NSAttributedString
}
