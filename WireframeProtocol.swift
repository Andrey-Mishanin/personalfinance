//
//  WireframeProtocol.swift
//  PersonalFinance
//
//  Created by Andrey Mishanin on 07/05/2017.
//  Copyright © 2017 Andrey Mishanin. All rights reserved.
//

import Foundation

public protocol WireframeProtocol: class {
  func presentEditTransactionUI(transaction: Transaction, completion: @escaping (Transaction) -> Void)
  func dismissTransactionUI(committing: Bool)
  func presentCategorySelectionUI(withSelectedCategory selectedCategory: TransactionCategory?,
                                  completion: @escaping (TransactionCategory?) -> Void)
  func presentAccountSelectionUI(withSelectedAccount selectedAccount: Account?,
                                 completion: @escaping (Account?) -> Void)
}
