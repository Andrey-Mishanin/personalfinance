//
//  AccountsUIPolicyTest.swift
//  PersonalFinance
//
//  Created by Andrey Mishanin on 06/03/2015.
//  Copyright (c) 2015 Andrey Mishanin. All rights reserved.
//

import XCTest
import PersonalFinanceCore_OSX

final class AccountsUIPolicyTests: XCTestCase {
  let policy: AccountsUIPolicy = AccountsUIPolicy()

  func testAsksToDisplayPromptWhenNoAccountsPresent() {
    let action = policy.decideActionForAccounts([])

    XCTAssertEqual(action, .displayAccountCreationPrompt)
  }

  func test_WhenAtLeastOneAccountPresent_DisplaysAccountList() {
    let accounts = [ testAccount ]

    let action = policy.decideActionForAccounts(accounts)

    XCTAssertEqual(action, .displayAccountList)
  }
}
