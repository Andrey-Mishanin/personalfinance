//
//  UIRouteClass.swift
//  PersonalFinance
//
//  Created by Andrey Mishanin on 09/03/2015.
//  Copyright (c) 2015 Andrey Mishanin. All rights reserved.
//

import XCTest
import PersonalFinanceCore_OSX

final class UIRouteTests: XCTestCase {
  private let route: UIRoute = UIRoute()

  func testDisplaysAddTransactionQuicklyScreenWhenAtLeastOneAccountPresent() {
    let accounts = [ testAccount ]

    let screen = route.startScreenForAccounts(accounts)

    XCTAssertEqual(screen, .addTransactionQuickly)
  }
}
