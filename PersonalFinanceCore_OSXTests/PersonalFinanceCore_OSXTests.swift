//
//  PersonalFinanceCore_OSXTests.swift
//  PersonalFinanceCore_OSXTests
//
//  Created by Andrey Mishanin on 10/04/2016.
//  Copyright © 2016 Andrey Mishanin. All rights reserved.
//

import XCTest
import PersonalFinanceCore_OSX

final class PersonalFinanceCore_OSXTests: XCTestCase {
//  func testSaver() {
//    let url = URL(fileURLWithPath: ProcessInfo.processInfo.arguments[1])
//
//    let db = openDatabase(url: url)
//    XCTAssertNotNil(db)
//
//    guard let database = db else {
//      return
//    }
//
//    let query = "SELECT * FROM expences LIMIT 10"
//    let transactions: [SaverTransaction]? = readRows(from: database, query: query)
//    XCTAssertNotNil(transactions)
//
//    guard let rows = transactions else {
//      return
//    }
//
//    for row in rows {
//      print(row)
//    }
//  }

  func testKoku() {
    let url = URL(fileURLWithPath: ProcessInfo.processInfo.arguments[1])

    let db = openDatabase(url: url)
    XCTAssertNotNil(db)

    guard let database = db else {
      return
    }

    let query = "SELECT ZFRTRANSACTION.Z_PK, ZACCOUNT, ZFRACCOUNT.ZNAME, ZDATE, ZDEPOSIT, ZWITHDRAWAL, ZFRTAG.Z_PK, ZFRTAG.ZNAME, ZINFO, ZNOTE FROM ZFRTRANSACTION JOIN ZFRACCOUNT ON ZFRTRANSACTION.ZACCOUNT = ZFRACCOUNT.Z_PK JOIN Z_11TRANSACTIONS ON ZFRTRANSACTION.Z_PK = Z_12TRANSACTIONS1 JOIN ZFRTAG ON Z_11TAGS = ZFRTAG.Z_PK LIMIT 10"
    let transactions: [KokuTransaction]? = readRows(from: database, query: query)

    if let rows = transactions {
      for row in rows {
        print(row)
      }
    }
  }
}

final class TableViewModelTests: XCTestCase {
  func test_WhenDoesNotHaveEditableRows_IndexOfFirstResponderIsNil() {
    let vm = TableViewModel(sections: [
      .init(rows: [
        .init(title: "".withAttributes([]),
              detail: "".withAttributes([]),
              style: .plain,
              id: 0)
      ], id: 0)
    ])

    XCTAssertNil(vm.indexPathOfFirstResponderRow)
  }

  func test_IndexPathOfFirstResponderRow() {
    let vm = TableViewModel(sections: [
      .init(rows: [
        .init(title: "".withAttributes([]),
              detail: "".withAttributes([]),
              style: .form(editableStyle: .text, isFirstResponder: false, badgeColor: nil),
              id: 0)
      ], id: 0),
      .init(rows: [
        .init(title: "".withAttributes([]),
              detail: "".withAttributes([]),
              style: .plain,
              id: 0),
        .init(title: "".withAttributes([]),
              detail: "".withAttributes([]),
              style: .form(editableStyle: .text, isFirstResponder: true, badgeColor: nil),
              id: 1)
      ], id: 1)
    ])

    XCTAssertEqual(vm.indexPathOfFirstResponderRow, IndexPath(item: 1, section: 1))
  }
}

let testAccount = Account(id: 0, name: "Account", type: .cash, currency: .britishPound)
let testRURAccount = Account(id: 1, name: "RUR Account", type: .savings, currency: .russianRouble)

let testTransactions = [
  Transaction(
    id: ID(value: 0, source: .personalFinance),
    amount: -42.3,
    date: Calendar.current.date(from: DateComponents(year: 2018, month: 1, day: 5))!,
    category: TransactionCategory(id: 0, name: "Health"),
    info: "Спазмалгон",
    account: testRURAccount
  ),
  Transaction(
    id: ID(value: 1, source: .personalFinance),
    amount: -2036,
    date: Calendar.current.date(from: DateComponents(year: 2019, month: 3, day: 6))!,
    category: TransactionCategory(id: 1, name: "Hotel"),
    account: testAccount
  ),
  Transaction(
    id: ID(value: 2, source: .personalFinance),
    amount: -480,
    date: Calendar.current.date(from: DateComponents(year: 2018, month: 1, day: 12))!,
    category: TransactionCategory(id: 0, name: "Health"),
    info: "Durex",
    account: testRURAccount
  )
]
