//
//  AttributedStringExtensionsTests.swift
//  PersonalFinance
//
//  Created by Andrey Mishanin on 28/04/2017.
//  Copyright © 2017 Andrey Mishanin. All rights reserved.
//

import XCTest
import PersonalFinanceCore_OSX

final class AttributedStringExtensionsTests: XCTestCase {
  func test_ConvertsAttributesToCocoaAttributes() {
    let anyFont = Font.systemFont(ofSize: 42)
    let anyAlignment = TextAlignment.center
    let anyTextColor = Color.gray
    let anyOffset = Float(-1)
    let attrString = AttributedString(string: "Test", attributes: [
      .font(anyFont),
      .paragraphStyle(anyAlignment),
      .textColor(anyTextColor),
      .baselineOffset(anyOffset),
    ])

    let expectedAttrs: [NSAttributedString.Key : Any] = [
      .font : anyFont,
      .paragraphStyle : ParagraphStyle.with(alignment: anyAlignment),
      .foregroundColor : anyTextColor,
      .baselineOffset : anyOffset,
    ]
    let expectedString = NSAttributedString(string: attrString.string,
                                            attributes: expectedAttrs)
    XCTAssertEqual(attrString.toCocoa, expectedString)
  }
}
