//
//  WireframeSpy.swift
//  PersonalFinanceCore_OSXTests
//
//  Created by Andrey Mishanin on 06/01/2018.
//  Copyright © 2018 Andrey Mishanin. All rights reserved.
//

import Foundation
import PersonalFinanceCore_OSX

final class WireframeSpy: WireframeProtocol {
  enum Event: Equatable {
    case presentEditTransactionUI(transaction: Transaction, completion: (Transaction) -> Void)
    case presentCategorySelectionUI(selectedCategory: TransactionCategory?, completion: (TransactionCategory?) -> Void)
    case presentAccountSelectionUI(selectedAccount: Account?, completion: (Account?) -> Void)
    case dismissTransactionUI(committing: Bool)

    static func ==(lhs: Event, rhs: Event) -> Bool {
      switch (lhs, rhs) {
      case let (.presentEditTransactionUI(transaction1, _), .presentEditTransactionUI(transaction2, _)):
        return transaction1 == transaction2
      case let (.presentCategorySelectionUI(category1, _), .presentCategorySelectionUI(category2, _)):
        return category1 == category2
      case let (.presentAccountSelectionUI(account1, _), .presentAccountSelectionUI(account2, _)):
        return account1 == account2
      case let (.dismissTransactionUI(committing1), .dismissTransactionUI(committing2)):
        return committing1 == committing2
      default:
        return false
      }
    }
  }

  var events: [Event] = []

  func presentEditTransactionUI(transaction: Transaction, completion: @escaping (Transaction) -> Void) {
    events.append(.presentEditTransactionUI(transaction: transaction, completion: completion))
  }

  func presentCategorySelectionUI(withSelectedCategory selectedCategory: TransactionCategory?,
                                  completion: @escaping (TransactionCategory?) -> Void) {
    events.append(.presentCategorySelectionUI(selectedCategory: selectedCategory, completion: completion))
  }

  func presentAccountSelectionUI(withSelectedAccount selectedAccount: Account?, completion: @escaping (Account?) -> Void) {
    events.append(.presentAccountSelectionUI(selectedAccount: selectedAccount, completion: completion))
  }

  func dismissTransactionUI(committing: Bool) {
    events.append(.dismissTransactionUI(committing: committing))
  }
}
