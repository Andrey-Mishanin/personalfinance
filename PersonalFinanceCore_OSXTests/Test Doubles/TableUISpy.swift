//
//  TableUISpy.swift
//  PersonalFinanceCore_OSXTests
//
//  Created by Andrey Mishanin on 25/12/2017.
//  Copyright © 2017 Andrey Mishanin. All rights reserved.
//

import Foundation
import PersonalFinanceCore_OSX

final class TableUISpy: TableUI {
  var model = TableViewModel(sections: [TableViewModel.Section(rows: [], id: 0)])
}
