//
//  DictionaryExtensionTests.swift
//  PersonalFinance
//
//  Created by Andrey Mishanin on 25/04/2017.
//  Copyright © 2017 Andrey Mishanin. All rights reserved.
//

import XCTest
import PersonalFinanceCore_OSX

final class DictionaryExtensionTests: XCTestCase {
  func test_MappingKeys() {
    let dict = [
      1 : "first",
      2 : "second",
    ]

    let expectedDict = [
      "1" : "first",
      "2" : "second",
    ]
    XCTAssertEqual(dict.mapKeys({ "\($0)" }), expectedDict)
  }
}
