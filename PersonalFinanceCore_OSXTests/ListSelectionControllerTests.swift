//
//  ListSelectionControllerTests.swift
//  PersonalFinanceCore_OSXTests
//
//  Created by Andrey Mishanin on 25/12/2017.
//  Copyright © 2017 Andrey Mishanin. All rights reserved.
//

import XCTest
import PersonalFinanceCore_OSX

final class ListSelectionControllerTests: XCTestCase {
  private var controller: ListSelectionController<TransactionCategory>!
  private let uiSpy = TableUISpy()

  func test_WhenUIAppears_SetsInitialViewModel() {
    setUp(withSelectedCategory: nil)

    controller.uiWillAppear()

    XCTAssertEqual(uiSpy.model, expectedInitialVM)
  }

  func test_WhenInitialisedWithoutSelectedCategory_ReturnsNilFromProperty() {
    setUp(withSelectedCategory: nil)

    XCTAssertEqual(controller.selectedItem, nil)
  }

  func test_WhenUIAppearsAndCategoryIsAlreadySelected_SetsInitialViewModelWithCheckmark() {
    let selectedIdx = 2
    setUp(withSelectedCategory: testCategories[selectedIdx])

    controller.uiWillAppear()

    XCTAssertEqual(uiSpy.model, makeExpectedInitialVM(selectedIdx: selectedIdx))
  }

  func test_WhenInitialisedWithSelectedCategory_ReturnsItFromProperty() {
    let selectedCategory = testCategories[2]
    setUp(withSelectedCategory: selectedCategory)

    XCTAssertEqual(controller.selectedItem, selectedCategory)
  }

  func test_WhenUIAppearsAndUnknownCategoryIsSelected_SetsInitialViewModel() {
    setUp(withSelectedCategory: TransactionCategory(id: 42, name: "WC"))

    controller.uiWillAppear()

    XCTAssertEqual(uiSpy.model, expectedInitialVM)
  }

  func test_WhenRowIsSelected_SetsViewModelWithSelection() {
    setUp(withSelectedCategory: nil)

    controller.uiWillAppear()
    let selectedIdxPath = IndexPath(item: 2, section: 0)
    controller.rowWasSelected(at: selectedIdxPath)

    XCTAssertEqual(uiSpy.model, makeExpectedInitialVM(selectedIdx: selectedIdxPath.item))
  }

  func test_WhenRowIsSelected_ReturnsCorrespondingCategoryFromProperty() {
    let selectedCategory = testCategories[2]
    setUp(withSelectedCategory: selectedCategory)

    let selectedIdxPath = IndexPath(item: 3, section: 0)
    controller.rowWasSelected(at: selectedIdxPath)

    XCTAssertEqual(controller.selectedItem, testCategories[selectedIdxPath.item])
  }

  private func setUp(withSelectedCategory selectedCategory: TransactionCategory?) {
    let model = ListSelectionController.Model(items: testCategories,
                                              selectedItem:selectedCategory)
    controller = ListSelectionController(model: model)
    controller.ui = uiSpy
  }
}

private let testCategoryNames = [
  "Health",
  "Hobby",
  "Hotel",
  "House",
  "Income"
]
private let testCategories = testCategoryNames.enumerated().map { TransactionCategory(id: $0, name: $1) }
private let expectedInitialVM = makeExpectedInitialVM(selectedIdx: nil)
private func makeExpectedInitialVM(selectedIdx: Int?) -> TableViewModel {
  return TableViewModel(sections: [
    TableViewModel.Section(rows: testCategoryNames.enumerated().map {
      TableViewModel.Row(title: $0.1.withAttributes([]),
                         detail: "".withAttributes([]),
                         style: .plain,
                         selectionStyle: .gray,
                         accessoryType: selectedIdx == $0.0 ? .checkmark : .none,
                         id: $0.0)
    }, id: 0)
  ])
}
