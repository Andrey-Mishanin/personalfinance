//
//  TransactionListControllerTests.swift
//  PersonalFinanceCore_OSXTests
//
//  Created by Andrey Mishanin on 06/01/2018.
//  Copyright © 2018 Andrey Mishanin. All rights reserved.
//

import XCTest
import PersonalFinanceCore_OSX

final class TransactionListControllerTests: XCTestCase {
  private let wireframeSpy = WireframeSpy()
  private let uiSpy = TableUISpy()
  private var controller: TransactionListController<FakeTransactionStore>!
  private var store: FakeTransactionStore!

  override func setUp() {
    setUpWith(transactions: testTransactions)
  }

  private func setUpWith(transactions: [Transaction]) {
    store = FakeTransactionStore(transactions: transactions)
    controller = TransactionListController(store: store,
                                           wireframe: wireframeSpy,
                                           colorForCategory: color(for:))
    store.observer = controller
    controller.ui = uiSpy
  }

  func test_WhenUIAppears_SetsVMForTransactions() {
    controller.uiWillAppear()

    XCTAssertEqual(uiSpy.model, expectedVM)
  }

  func test_WhenNewTransactionIsAppended_AddsNewRowInUI() {
    setUpWith(transactions: [testTransactions[0], testTransactions[1]])

    store.transactions.append(testTransactions[2])

    XCTAssertEqual(uiSpy.model, expectedVM)
  }

  func test_DepositTransactionsAreHighlighted() {
    var depositTransaction = testTransactions[0]
    depositTransaction.amount.negate()
    setUpWith(transactions: [depositTransaction])

    controller.uiWillAppear()

    let expected = makeTableVM([
      (name: "January 2018", itemVMs: [
        (info: "Спазмалгон", amount: "+₽42.30", color: .purple),
      ])
    ])
    XCTAssertEqual(uiSpy.model, expected)
  }

  func test_WhenRowIsSelected_AsksWireframeToDisplayCorrespondingTransaction() {
    controller.uiWillAppear()

    controller.rowWasSelected(at: IndexPath(item: 0, section: 1))

    XCTAssertEqual(wireframeSpy.events.last, .presentEditTransactionUI(transaction: testTransactions[2],
                                                                       completion: { _ in }))
  }

  func test_WhenTransactionFinishedEditing_UpdatesVM() {
    controller.uiWillAppear()
    controller.rowWasSelected(at: IndexPath(item: 1, section: 1))
    var editedTransaction: Transaction!
    var editingCompletion: ((Transaction) -> Void)!
    if case let .presentEditTransactionUI(transaction, completion) = wireframeSpy.events.last! {
      editedTransaction = transaction
      editingCompletion = completion
    }
    editedTransaction.amount -= 1

    editingCompletion(editedTransaction)

    let expectedVMAfterEditing = makeTableVM([
      (name: "March 2019", itemVMs: [
        (info: "", amount: "£2,036.00", color: .blue),
      ]),
      (name: "January 2018", itemVMs: [
        (info: "Durex", amount: "₽480.00", color: .purple),
        (info: "Спазмалгон", amount: "₽43.30", color: .purple),
      ])
    ])
    XCTAssertEqual(uiSpy.model, expectedVMAfterEditing)
  }
}

private let expectedVM = makeTableVM([
  (name: "March 2019", itemVMs: [
    (info: "", amount: "£2,036.00", color: .blue),
  ]),
  (name: "January 2018", itemVMs: [
    (info: "Durex", amount: "₽480.00", color: .purple),
    (info: "Спазмалгон", amount: "₽42.30", color: .purple),
  ])
])

private typealias TransactionVM = (info: String, amount: String, color: Color)
private typealias ListVM = [ (name: String, itemVMs: [TransactionVM]) ]
private func makeTableVM(_ listVM: ListVM) -> TableViewModel {
  let sections = listVM.enumerated().map {
    TableViewModel.Section(name: $0.1.name, rows: $0.1.itemVMs.enumerated().map {
      TableViewModel.Row(title: $0.1.info.withAttributes([]),
                         detail: $0.1.amount.withAttributes(rightAlignedAttrs(for: $0.1.amount)),
                         style: .form(editableStyle: .none, isFirstResponder: false, badgeColor: $0.1.color),
                         selectionStyle: .gray,
                         accessoryType: .disclosureIndicator,
                         id: $0.0)
    }, id: $0.0)
  }
  return TableViewModel(sections: sections)
}

private class FakeTransactionStore: TransactionStoreProtocol {
  var nextTransactionID: ID {
    ID(value: transactions.count, source: .personalFinance)
  }

  unowned var observer: TransactionStoreObserver!

  var transactions: [Transaction] {
    didSet {
      observer.transactionStoreDidLoad(transactions: transactions)
    }
  }

  init(transactions: [Transaction]) {
    self.transactions = transactions
  }

  func loadAllTransactions() {
    observer.transactionStoreDidLoad(transactions: transactions)
  }

  func update(transaction: Transaction) {
    let idx = transactions.firstIndex(where: { $0.id == transaction.id })!
    transactions[idx] = transaction
  }
}

private func rightAlignedAttrs(for amount: String) -> [AttributedString.Attribute] {
  [
    .font(.boldSystemFont(ofSize: 17)),
    .paragraphStyle(.right),
  ] + (amount.starts(with: "+") ? [.textColor(.systemGreen)] : [])
}

private func color(for category: TransactionCategory) -> Color {
  let colors: [Int : Color] = [
    0 : .purple,
    1 : .blue,
  ]
  return colors[category.id]!
}
