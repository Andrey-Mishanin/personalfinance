//
//  EditTransactionControllerTests.swift
//  PersonalFinance
//
//  Created by Andrey Mishanin on 24/04/2017.
//  Copyright © 2017 Andrey Mishanin. All rights reserved.
//

import XCTest
import PersonalFinanceCore_OSX

final class EditTransactionControllerTests: XCTestCase {
  private var controller: EditTransactionController!
  private let wireframeSpy = WireframeSpy()
  private let uiSpy = TableUISpy()

  override func setUp() {
    super.setUp()
    setUp(isNewTransaction: true, setCommitEnabled: { _ in })
  }

  private func setUp(isNewTransaction: Bool, setCommitEnabled: @escaping (Bool) -> Void) {
    let params: EditTransactionController.Params = isNewTransaction ?
      .newTransaction(provisionalID: testProvisionalID) :
      .existingTransaction(transaction: testTransactions[0])
    controller = EditTransactionController(params: params,
                                           wireframe: wireframeSpy,
                                           setCommitEnabled: setCommitEnabled)
    controller.ui = uiSpy
  }

  func test_WhenUIIsGoingToAppear_SetsInitialViewModel() {
    controller.uiWillAppear()

    XCTAssertEqual(uiSpy.model, expectedInitialVM)
  }

  func test_WhenCategoryRowIsSelected_AsksWireframeToPresentCategorySelectionUIWithNoCategoryAtFirst() {
    controller.uiWillAppear()

    controller.rowWasSelected(at: IndexPath(item: Row.category.rawValue, section: 0))

    let expectedEvents = [WireframeSpy.Event.presentCategorySelectionUI(selectedCategory: nil, completion: { _ in })]
    XCTAssertEqual(wireframeSpy.events, expectedEvents)
  }

  func test_WhenNotCategoryRowIsSelected_DoesNotAskWireframeToPresentCategorySelectionUI() {
    controller.uiWillAppear()

    for row in [Row.amount, Row.date] {
      controller.rowWasSelected(at: IndexPath(item: row.rawValue, section: 0))

      XCTAssert(wireframeSpy.events.isEmpty)
    }
  }

  func test_WhenAccountRowIsSelected_AsksWireframeToPresentAccountSelectionUIWithNoAccountAtFirst() {
    controller.uiWillAppear()

    controller.rowWasSelected(at: IndexPath(item: Row.account.rawValue, section: 0))

    let expectedEvents = [WireframeSpy.Event.presentAccountSelectionUI(selectedAccount: nil, completion: { _ in })]
    XCTAssertEqual(wireframeSpy.events, expectedEvents)
  }

  func test_WhenCategoryRowIsSelected_AsksWireframeToPresentCategorySelectionUIWithPreviouslySelectedCategory() {
    controller.uiWillAppear()
    let expectedCategory = TransactionCategory(id: 42, name: "Health")
    select(category: expectedCategory)

    controller.rowWasSelected(at: IndexPath(item: Row.category.rawValue, section: 0))

    let expectedLastEvent = WireframeSpy.Event.presentCategorySelectionUI(selectedCategory: expectedCategory,
                                                                          completion: { _ in })
    XCTAssertEqual(wireframeSpy.events.last!, expectedLastEvent)
  }

  func test_WhenAccountRowIsSelected_AsksWireframeToPresentAccountSelectionUIWithPreviouslySelectedCategory() {
    controller.uiWillAppear()
    select(account: testAccount)

    controller.rowWasSelected(at: IndexPath(item: Row.account.rawValue, section: 0))

    let expectedLastEvent = WireframeSpy.Event.presentAccountSelectionUI(selectedAccount: testAccount,
                                                                         completion: { _ in })
    XCTAssertEqual(wireframeSpy.events.last!, expectedLastEvent)
  }

  func test_WhenCategoryIsSelected_UpdatesUIWithCategoryName() {
    controller.uiWillAppear()
    let expectedCategory = TransactionCategory(id: 42, name: "Health")

    select(category: expectedCategory)

    let categoryRow = uiSpy.model.sections[0].rows[Row.category.rawValue]
    XCTAssertEqual(categoryRow.detail, expectedCategory.name.withAttributes(leftAlignedAttrs))
  }

  func test_WhenAccountIsSelected_UpdatesUIWithAccountName() {
    controller.uiWillAppear()

    select(account: testAccount)

    let accountRow = uiSpy.model.sections[0].rows[Row.account.rawValue]
    XCTAssertEqual(accountRow.detail, testAccount.name.withAttributes(leftAlignedAttrs))
  }

  func test_WhenUIAppears_PreservesPreviouslyEnteredAmount() {
    controller.uiWillAppear()
    let newAmount = "42.20"

    controller.row(at: Row.amount.rawValue, didChangeDetailTextTo: newAmount)
    controller.uiWillAppear()

    XCTAssertEqual(uiSpy.model.sections[0].rows[Row.amount.rawValue].detail, newAmount.withAttributes(leftAlignedAttrs))
  }

  func test_WhenUIAppearsAndLastChangedRowWasNotAmount_PreservesPreviouslyEnteredAmount() {
    controller.uiWillAppear()
    let newAmount = "42.20"
    controller.row(at: Row.amount.rawValue, didChangeDetailTextTo: newAmount)
    let newDate = "20/12/201"

    controller.row(at: Row.date.rawValue, didChangeDetailTextTo: newDate)
    controller.uiWillAppear()

    XCTAssertEqual(uiSpy.model.sections[0].rows[Row.amount.rawValue].detail, newAmount.withAttributes(leftAlignedAttrs))
  }

  func test_WhenAmountCategoryAndAccountAreSpecified_ReturnsTransaction() {
    controller.uiWillAppear()
    // Amount
    controller.row(at: Row.amount.rawValue, didChangeDetailTextTo: "42.20")
    // Category
    let expectedCategory = TransactionCategory(id: 42, name: "Health")
    select(category: expectedCategory)
    // Account
    select(account: testAccount)
    // Info
    let info = "Starbucks"
    controller.row(at: Row.info.rawValue, didChangeDetailTextTo: info)

    let expectedTransaction = Transaction(id: testProvisionalID,
                                          amount: 42.20,
                                          date: Date(),
                                          category: expectedCategory,
                                          info: info,
                                          account: testAccount)
    XCTAssertEqual(controller.transaction!, expectedTransaction)
  }

  func test_WhenInitialisedWithExistingTransaction_SetsCorrespondingVM() {
    setUp(isNewTransaction: false, setCommitEnabled: { _ in })

    controller.uiWillAppear()

    let expectedVM = makeVM(amount: "42.30", date: testTransactions[0].date, category: "Health", account: "RUR Account", info: "Спазмалгон")
    XCTAssertEqual(uiSpy.model, expectedVM)
  }

  func test_WhenInitialisedWithExistingTransaction_PreservesTransactionID() {
    setUp(isNewTransaction: false, setCommitEnabled: { _ in })

    XCTAssertEqual(controller.transaction?.id, testTransactions[0].id)
  }

  func test_WhenInitialisedWithExistingTransaction_PreservesDate() {
    setUp(isNewTransaction: false, setCommitEnabled: { _ in })

    XCTAssertEqual(controller.transaction?.date, testTransactions[0].date)
  }

  func test_WhenUIAppears_PreservesPreviouslyEnteredInfo() {
    controller.uiWillAppear()
    let newInfo = "Starbucks"

    controller.row(at: Row.info.rawValue, didChangeDetailTextTo: newInfo)
    controller.uiWillAppear()

    XCTAssertEqual(uiSpy.model.sections[0].rows[Row.info.rawValue].detail, newInfo.withAttributes(leftAlignedAttrs))
  }

  func test_WhenUIAppearsForNewTransaction_CommitIsDisabled() {
    var enabled: Bool!
    setUp(isNewTransaction: true, setCommitEnabled: { enabled = $0 })

    controller.uiWillAppear()

    XCTAssertFalse(enabled)
  }

  func test_WhenUIAppearsForExistingTransaction_CommitIsEnabled() {
    var enabled: Bool!
    setUp(isNewTransaction: false, setCommitEnabled: { enabled = $0 })

    controller.uiWillAppear()

    XCTAssert(enabled)
  }

  func test_WhenOnlyAmountIsSpecified_CommitIsDisabled() {
    var enabled: Bool!
    setUp(isNewTransaction: true, setCommitEnabled: { enabled = $0 })
    controller.uiWillAppear()

    controller.row(at: Row.amount.rawValue, didChangeDetailTextTo: "42.20")

    XCTAssertFalse(enabled)
  }

  func test_WhenAmountIsValidAfterEditingForExistingTransaction_CommitIsEnabled() {
    var enabled: Bool!
    setUp(isNewTransaction: false, setCommitEnabled: { enabled = $0 })
    controller.uiWillAppear()

    controller.row(at: Row.amount.rawValue, didChangeDetailTextTo: "42.20")

    XCTAssert(enabled)
  }

  func test_WhenAmountIsInvalidAfterEditingForExistingTransaction_CommitIsDisabled() {
    var enabled: Bool!
    setUp(isNewTransaction: false, setCommitEnabled: { enabled = $0 })
    controller.uiWillAppear()

    controller.row(at: Row.amount.rawValue, didChangeDetailTextTo: "")

    XCTAssertFalse(enabled)
  }

  func test_WhenCategoryIsInvalidAfterSelectionForExistingTransaction_CommitIsDisabled() {
    var enabled: Bool!
    setUp(isNewTransaction: false, setCommitEnabled: { enabled = $0 })
    controller.uiWillAppear()

    select(category: nil)

    XCTAssertFalse(enabled)
  }

  func test_WhenCategoryIsValidAfterSelectionForExistingTransaction_CommitIsEnabled() {
    var enabled: Bool!
    setUp(isNewTransaction: false, setCommitEnabled: { enabled = $0 })
    controller.uiWillAppear()

    select(category: TransactionCategory(id: 42, name: "Health"))

    XCTAssert(enabled)
  }

  func test_WhenOnlyCategoryIsSpecified_CommitIsDisabled() {
    var enabled: Bool!
    setUp(isNewTransaction: true, setCommitEnabled: { enabled = $0 })
    controller.uiWillAppear()

    select(category: TransactionCategory(id: 42, name: "Health"))

    XCTAssertFalse(enabled)
  }

  func test_WhenAccountIsInvalidAfterSelectionForExistingTransaction_CommitIsDisabled() {
    var enabled: Bool!
    setUp(isNewTransaction: false, setCommitEnabled: { enabled = $0 })
    controller.uiWillAppear()

    select(account: nil)

    XCTAssertFalse(enabled)
  }

  func test_WhenAccountIsValidAfterSelectionForExistingTransaction_CommitIsEnabled() {
    var enabled: Bool!
    setUp(isNewTransaction: false, setCommitEnabled: { enabled = $0 })
    controller.uiWillAppear()

    select(account: testAccount)

    XCTAssert(enabled)
  }

  func test_WhenOnlyAccountIsSpecified_CommitIsDisabled() {
    var enabled: Bool!
    setUp(isNewTransaction: true, setCommitEnabled: { enabled = $0 })
    controller.uiWillAppear()

    select(account: testAccount)

    XCTAssertFalse(enabled)
  }

  func test_WhenOnlyAmountAndCategoryAreSpecified_CommitIsDisabled() {
    var enabled: Bool!
    setUp(isNewTransaction: true, setCommitEnabled: { enabled = $0 })
    controller.uiWillAppear()

    controller.row(at: Row.amount.rawValue, didChangeDetailTextTo: "42.20")
    select(category: TransactionCategory(id: 42, name: "Health"))

    XCTAssertFalse(enabled)
  }

  func test_WhenOnlyAmountAndAccountAreSpecified_CommitIsDisabled() {
    var enabled: Bool!
    setUp(isNewTransaction: true, setCommitEnabled: { enabled = $0 })
    controller.uiWillAppear()

    controller.row(at: Row.amount.rawValue, didChangeDetailTextTo: "42.20")
    select(account: testAccount)

    XCTAssertFalse(enabled)
  }

  func test_WhenCategoryIsInvalidAfterSelectionForNewTransactionWithAmount_CommitIsDisabled() {
    var enabled: Bool!
    setUp(isNewTransaction: true, setCommitEnabled: { enabled = $0 })
    controller.uiWillAppear()
    controller.row(at: Row.amount.rawValue, didChangeDetailTextTo: "42.20")
    select(category: nil)

    controller.uiWillAppear()

    XCTAssertFalse(enabled)
  }

  func test_WhenAccountIsInvalidAfterSelectionForNewTransactionWithAmount_CommitIsDisabled() {
    var enabled: Bool!
    setUp(isNewTransaction: true, setCommitEnabled: { enabled = $0 })
    controller.uiWillAppear()
    controller.row(at: Row.amount.rawValue, didChangeDetailTextTo: "42.20")
    select(category: TransactionCategory(id: 42, name: "Health"))
    select(account: nil)

    controller.uiWillAppear()

    XCTAssertFalse(enabled)
  }

  func test_WhenAccountIsNotSpecifiedForNewTransactionWithAmount_CommitIsDisabled() {
    var enabled: Bool!
    setUp(isNewTransaction: true, setCommitEnabled: { enabled = $0 })
    controller.uiWillAppear()
    select(category: TransactionCategory(id: 42, name: "Health"))

    controller.row(at: Row.amount.rawValue, didChangeDetailTextTo: "42.20")

    XCTAssertFalse(enabled)
  }

  private func select(category: TransactionCategory?) {
    controller.rowWasSelected(at: IndexPath(item: Row.category.rawValue, section: 0))
    var categorySelectionCompletion: ((TransactionCategory?) -> Void)!
    if case let .presentCategorySelectionUI(_, completion) = wireframeSpy.events.last! {
      categorySelectionCompletion = completion
    }
    categorySelectionCompletion(category)
  }

  private func select(account: Account?) {
    controller.rowWasSelected(at: IndexPath(item: Row.account.rawValue, section: 0))
    var accountSelectionCompletion: ((Account?) -> Void)!
    if case let .presentAccountSelectionUI(_, completion) = wireframeSpy.events.last! {
      accountSelectionCompletion = completion
    }
    accountSelectionCompletion(account)
  }
}

private let expectedInitialVM = makeVM(amount: "", date: Date(), category: "", account: "default", info: "")

private func makeVM(amount: String, date: Date, category: String, account: String, info: String) -> TableViewModel {
  let rightAlignedAttrs: [AttributedString.Attribute] = [
    .font(.boldSystemFont(ofSize: 17)),
    .paragraphStyle(.right)
  ]
  let dateFormatter = DateFormatter()
  dateFormatter.dateStyle = .short
  dateFormatter.timeStyle = .none
  return TableViewModel(sections: [
    TableViewModel.Section(rows: [
      TableViewModel.Row(title: NSLocalizedString("Amount", comment: "").withAttributes(rightAlignedAttrs),
                         detail: amount.withAttributes(leftAlignedAttrs),
                         style: .form(editableStyle: .currency, isFirstResponder: true, badgeColor: nil),
                         id: 0),
      TableViewModel.Row(title: NSLocalizedString("Date", comment: "").withAttributes(rightAlignedAttrs),
                         detail: dateFormatter.string(from: date).withAttributes(leftAlignedAttrs),
                         style: .form(editableStyle: .none, isFirstResponder: false, badgeColor: nil),
                         id: 1),
      TableViewModel.Row(title: NSLocalizedString("Category", comment: "").withAttributes(rightAlignedAttrs),
                         detail: category.withAttributes(leftAlignedAttrs),
                         style: .form(editableStyle: .none, isFirstResponder: false, badgeColor: nil),
                         selectionStyle: .gray,
                         accessoryType: .disclosureIndicator,
                         id: 2),
      TableViewModel.Row(title: NSLocalizedString("Account", comment: "").withAttributes(rightAlignedAttrs),
                         detail: account.withAttributes(leftAlignedAttrs),
                         style: .form(editableStyle: .none, isFirstResponder: false, badgeColor: nil),
                         selectionStyle: .gray,
                         accessoryType: .disclosureIndicator,
                         id: 3),
      TableViewModel.Row(title: NSLocalizedString("Info", comment: "").withAttributes(rightAlignedAttrs),
                         detail: info.withAttributes(leftAlignedAttrs),
                         style: .form(editableStyle: .text, isFirstResponder: false, badgeColor: nil),
                         id: 4),
    ], id: 0)
  ])
}

private let leftAlignedAttrs: [AttributedString.Attribute] = [
  .font(.systemFont(ofSize: 17)),
  .paragraphStyle(.left),
  .textColor(.gray),
  .baselineOffset(-1),
]

private enum Row: Int {
  case amount
  case date
  case category
  case account
  case info
}

private let testProvisionalID = ID(value: 44, source: .personalFinance)
