//
//  EditTransactionView.swift
//  PersonalFinance
//
//  Created by Andrey Mishanin on 22/11/2020.
//  Copyright © 2020 Andrey Mishanin. All rights reserved.
//

import Foundation
import SwiftUI

struct EditTransactionView: View {
  struct Model {
    var amount: Decimal? = nil
    var date: Date
    var info = ""
    var selectedCategoryIdx = 0
    var selectedAccountIdx = 0
  }

  @State var model: Model

  var body: some View {
    let doneButton = Button("Done", action: {})
    let cancelButton = Button("Cancel", action: {})
    return NavigationView {
      Form {
        TextField("Amount", value: $model.amount, formatter: currencyFormatter)
        DatePicker("Date", selection: $model.date, displayedComponents: [.date])
        Picker("Category", selection: $model.selectedCategoryIdx) {
          ForEach(testCategories.indices) {
            Text(testCategories[$0].name)
          }
        }
        Picker("Account", selection: $model.selectedAccountIdx) {
          ForEach(testAccounts.indices) {
            Text(testAccounts[$0].name)
          }
        }
        TextField("Info", text: $model.info)
      }
    }
    .navigationBarItems(leading: doneButton, trailing: cancelButton)
  }
}

struct EditTransactionView_Previews: PreviewProvider {
  static var previews: some View {
    EditTransactionView(model: .init(amount: nil, date: Date(), info: ""))
    EditTransactionView(model: .init(amount: -42.3, date: Calendar.current.date(from: DateComponents(year: 2018, month: 1, day: 5))!, info: "Спазмалгон"))
  }
}

private let dateFormatter: DateFormatter = {
  let formatter = DateFormatter()
  formatter.dateStyle = .short
  formatter.timeStyle = .none
  return formatter
}()

private let currencyFormatter: NumberFormatter = {
  let formatter = NumberFormatter()
  formatter.maximumFractionDigits = 2
  formatter.minimumFractionDigits = 2
  formatter.minusSign = ""
  return formatter
}()

let testCategories = [
  "Health",
  "Hobby",
  "Hotel",
  "House",
  "Income"
].enumerated().map { TransactionCategory(id: $0, name: $1) }

let testAccounts = [
  ("Saver", .russianRouble),
  ("Альфа", .russianRouble),
  ("Наличные", .britishPound),
  ("Райффайзен", .russianRouble),
  ("Квартира", .russianRouble),
  ].enumerated().map { (arg: (Int, (String, Currency))) in
    return Account(id: arg.0, name: arg.1.0, type: .cash, currency: arg.1.1)
}
