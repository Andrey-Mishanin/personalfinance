//
//  TableUI.swift
//  PersonalFinance
//
//  Created by Andrey Mishanin on 24/04/2017.
//  Copyright © 2017 Andrey Mishanin. All rights reserved.
//

import Foundation

public protocol TableUI: class {
  var model: TableViewModel { get set }
}
