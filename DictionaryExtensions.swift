//
//  DictionaryExtensions.swift
//  PersonalFinance
//
//  Created by Andrey Mishanin on 25/04/2017.
//  Copyright © 2017 Andrey Mishanin. All rights reserved.
//

import Foundation

public extension Dictionary {
  func mapKeys<U: Hashable>(_ f: (Key) -> U) -> [U : Value] {
    return Dictionary<U, Value>(uniqueKeysWithValues: map { (f($0.key), $0.value) })
  }
}
