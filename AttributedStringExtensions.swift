//
//  AttributedStringExtensions.swift
//  PersonalFinance
//
//  Created by Andrey Mishanin on 25/04/2017.
//  Copyright © 2017 Andrey Mishanin. All rights reserved.
//

import Foundation

private extension AttributedString.Attribute {
  var toCocoaDictionary: [NSAttributedString.Key : Any] {
    switch self {
    case let .font(font):
      return [.font : font]
    case let .paragraphStyle(alignment):
      return [.paragraphStyle : ParagraphStyle.with(alignment: alignment)]
    case let .textColor(color):
      return [.foregroundColor : color]
    case let .baselineOffset(offset):
      return [.baselineOffset : offset]
    }
  }
}

public extension AttributedString {
  var toCocoa: NSAttributedString {
    return NSAttributedString(string: string,
                              attributes: convertAttributesToCocoa(attributes))
  }
}

public func convertAttributesToCocoa(_ attributes: [AttributedString.Attribute]) -> [NSAttributedString.Key : Any] {
  return attributes.reduce(into: [:], { $0.merge($1.toCocoaDictionary, uniquingKeysWith: { (_, new) in new }) })
}
