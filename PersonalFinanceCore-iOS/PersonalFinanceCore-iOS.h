//
//  PersonalFinanceCore-iOS.h
//  PersonalFinanceCore-iOS
//
//  Created by Andrey Mishanin on 10/04/2016.
//  Copyright © 2016 Andrey Mishanin. All rights reserved.
//

#import <UIKit/UIKit.h>

//! Project version number for PersonalFinanceCore-iOS.
FOUNDATION_EXPORT double PersonalFinanceCore_iOSVersionNumber;

//! Project version string for PersonalFinanceCore-iOS.
FOUNDATION_EXPORT const unsigned char PersonalFinanceCore_iOSVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <PersonalFinanceCore_iOS/PublicHeader.h>


