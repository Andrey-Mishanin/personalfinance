//
//  PublishingTableUI.swift
//  PersonalFinance
//
//  Created by Andrey Mishanin on 15/11/2020.
//  Copyright © 2020 Andrey Mishanin. All rights reserved.
//

import Foundation

public final class ObservableTableUI: TableUI, ObservableObject {
  @Published public var model = TableViewModel(sections: [TableViewModel.Section(rows: [], id: 0)])
  unowned let observer: TableUIObserver

  public init(observer: TableUIObserver) {
    self.observer = observer
  }
}
