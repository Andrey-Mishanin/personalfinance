//
//  TransactionCategory.swift
//  PersonalFinance
//
//  Created by Andrey Mishanin on 07/05/2017.
//  Copyright © 2017 Andrey Mishanin. All rights reserved.
//

import Foundation

public struct TransactionCategory: Equatable {
  public let id: Int
  public let name: String

  public init(id: Int, name: String) {
    self.id = id
    self.name = name
  }
}

extension TransactionCategory: Named { }
