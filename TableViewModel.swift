//
//  TableViewModel.swift
//  PersonalFinance
//
//  Created by Andrey Mishanin on 05/04/2017.
//  Copyright © 2017 Andrey Mishanin. All rights reserved.
//

import Foundation

public enum EditableStyle {
  case none
  case currency
  case date
  case text

  public var isEditable: Bool {
    return self != .none
  }
}

public struct TableViewModel: Equatable {
  public struct Row: Equatable, Identifiable {
    public enum Style: Equatable {
      case plain
      case form(editableStyle: EditableStyle, isFirstResponder: Bool, badgeColor: Color?)

      var isFirstResponder: Bool {
        if case .form(_, true, _) = self { return true } else { return false }
      }

      var badgeColor: Color? {
        if case let .form(_, _, color) = self { return color }
        return nil
      }
    }

    public enum SelectionStyle: Equatable {
      case none
      case gray
    }

    public enum AccessoryType: Equatable {
      case none
      case disclosureIndicator
      case checkmark
    }

    public let title: AttributedString
    public let detail: AttributedString
    public let style: Style
    public let selectionStyle: SelectionStyle
    public let accessoryType: AccessoryType
    public let id: Int

    public init(title: AttributedString,
                detail: AttributedString,
                style: Style,
                selectionStyle: SelectionStyle = .none,
                accessoryType: AccessoryType = .none,
                id: Int) {
      switch style {
      case .form(.none, true, _):
        preconditionFailure("Can't have non-editable row set to first responder")
      default:
        break
      }

      self.title = title
      self.detail = detail
      self.style = style
      self.selectionStyle = selectionStyle
      self.accessoryType = accessoryType
      self.id = id
    }
  }

  public struct Section: Equatable, Identifiable {
    public let name: String?
    public let rows: [Row]
    public let id: Int

    public init(name: String? = nil, rows: [Row], id: Int) {
      let numberOfFirstResponderRows = rows.count(where: { $0.style.isFirstResponder })
      precondition(numberOfFirstResponderRows <= 1, "Table must have no more than one first responder row")
      
      self.name = name
      self.rows = rows
      self.id = id
    }

    public var indexOfFirstResponderRow: Int? {
      return zip(rows.indices, rows).first(where: { $0.1.style.isFirstResponder })?.0
    }
  }

  public let sections: [Section]

  public init(sections: [Section]) {
    self.sections = sections
  }

  public var indexPathOfFirstResponderRow: IndexPath? {
    sections
      .enumerated()
      .map { ($0.0, $0.1.indexOfFirstResponderRow) }
      .first(where: { $0.1 != nil })
      .map { IndexPath(item: $0.1!, section: $0.0)}
  }
}
